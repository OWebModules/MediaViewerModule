﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Primitives
{
    public enum MultimediaItemType
    {
        Image = 0,
        Audio = 1,
        PDF = 2,
        Video = 4,
        VideoAndAudio = 5,
        Unknown = 100
    }

    public static class MediaTypes
    {
        public static string MediaType_MP3
        {
            get
            {
                return "audio/mpeg";
            }
        }

        public static string MediaType_WAV
        {
            get
            {
                return "audio/wav";
            }
        }

        public static string ExtensionMP3
        {
            get
            {
                return "mp3";
            }
        }

        public static string ExtensionWAV
        {
            get
            {
                return "wav";
            }
        }

        public static string ExtensionPDF
        {
            get
            {
                return "pdf";
            }
        }

        public static string MediaType_PDF
        {
            get
            {
                return "application/pdf";
            }
        }

        public static string ExtensionJPG
        {
            get
            {
                return "jpg";
            }
        }

        public static string ExtensionBMP
        {
            get
            {
                return "bmp";
            }
        }

        public static string ExtensionTIFF
        {
            get
            {
                return "tiff";
            }
        }

        public static string ExtensionPNG
        {
            get
            {
                return "png";
            }
        }

        public static string ExtensionJPEG
        {
            get
            {
                return "jpeg";
            }
        }

        public static string ExtensionGIF
        {
            get
            {
                return "gif";
            }
        }

        public static string ExtensionWMV
        {
            get
            {
                return "wmv";
            }
        }

        public static string MediaType_WMV
        {
            get
            {
                return "video/x-ms-wmv";
            }
        }

        public static string MediaType_AVI
        {
            get
            {
                return "video/x-msvideo";
            }
        }

        public static string ExtensionAVI
        {
            get
            {
                return "avi";
            }
        }

        public static string ExtensionMP4
        {
            get
            {
                return "mp4";
            }
        }

        public static string MediaType_MP4
        {
            get
            {
                return "video/mp4";
            }
        }

        public static string ExtensionMOD
        {
            get
            {
                return "mod";
            }
        }

        public static string ExtensionMTS
        {
            get
            {
                return "mts";
            }
        }

        public static string Extension3GP
        {
            get
            {
                return "3gp";
            }
        }

        public static List<string> VideoExtensions
        {
            get
            {
                var result = new List<string>
                {
                    Extension3GP,
                    ExtensionAVI,
                    ExtensionMOD,
                    ExtensionMP4,
                    ExtensionMTS,
                    ExtensionWMV
                };

                return result;
            }
        }

        public static List<string> ImageExtensions
        {
            get
            {
                var result = new List<string>
                {
                    ExtensionJPG,
                    ExtensionBMP,
                    ExtensionTIFF,
                    ExtensionPNG,
                    ExtensionJPEG,
                    ExtensionGIF
                };

                return result;
            }
        }

        public static List<string> AudioExtensions
        {
            get
            {
                var result = new List<string>
                {
                    ExtensionMP3,
                    ExtensionWAV
                };

                return result;
            }

        }

        public static MultimediaItemType GetMediaTypeOfFile(string fileName)
        {
            var extension = System.IO.Path.GetExtension(fileName).Replace(".","").ToLower();

            if (extension == ExtensionPDF)
            {
                return MultimediaItemType.PDF;
            }

            if (ImageExtensions.Contains(extension))
            {
                return MultimediaItemType.Image;
            }

            if (AudioExtensions.Contains(extension))
            {
                return MultimediaItemType.Audio;
            }

            if (VideoExtensions.Contains(extension))
            {
                return MultimediaItemType.Video;
            }

            return MultimediaItemType.Unknown;
        }

        public static string GetMimeTypeOfFile(string fileName)
        {
            var extension = System.IO.Path.GetExtension(fileName).Replace(".", "").ToLower();
            return GetMediaType(extension);
        }

        public static string GetMediaType(string extension)
        {
            if (extension.ToLower() == ExtensionAVI.ToLower())
            {
                return MediaType_AVI;
            }

            if (extension.ToLower() == ExtensionMP3.ToLower())
            {
                return MediaType_MP3;
            }

            if (extension.ToLower() == ExtensionMP4.ToLower())
            {
                return MediaType_MP4;
            }

            if (extension.ToLower() == ExtensionWAV.ToLower())
            {
                return MediaType_WAV;
            }

            if (extension.ToLower() == ExtensionWMV.ToLower())
            {
                return MediaType_WMV;
            }

            if (extension.ToLower() == ExtensionPDF.ToLower())
            {
                return MediaType_PDF;
            }

            return null;
        }

    }
}
