﻿using MediaStore_Module;
using MediaStore_Module.Models;
using MediaViewerModule.Factories;
using MediaViewerModule.Models;
using MediaViewerModule.Primitives;
using MediaViewerModule.Services;
using OModulesMediaService;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Connectors
{
    public class MediaViewerConnector : AppController
    {
        private object connectorLocker = new object();

        private ElasticAgentService elasticAgentService;
        private MediaItemListFactory factory;

        public static clsOntologyItem ClassImages
        {
            get
            {
                return Config.LocalData.Class_Images__Graphic_;
            }
        }

        public static clsOntologyItem ClassPDF
        {
            get
            {
                return Config.LocalData.Class_PDF_Documents;
            }
        }

        public static clsOntologyItem ClassMediaItems
        {
            get
            {
                return Config.LocalData.Class_Media_Item;
            }
        }

        public static clsOntologyItem ClassFile
        {
            get
            {
                return Config.LocalData.Class_File;
            }
        }

        public static clsOntologyItem RelationTypeBelongingSource
        {
            get
            {
                return Config.LocalData.RelationType_belonging_Source;
            }
        }

        private ResultMediaListItem mediaListItems;
        public ResultMediaListItem MediaListItems
        {
            get
            {
                lock (connectorLocker)
                {
                    return mediaListItems;
                }

            }
            set
            {
                lock (connectorLocker)
                {
                    mediaListItems = value;
                }
            }
        }

        public ResultMediaListItem GetEmptyMediaList()
        {
            var result = new ResultMediaListItem
            {
                Result = Globals.LState_Success.Clone(),
                MediaListItems = new List<MediaListItem>()
            };

            return result;
        }

        public async Task<ResultMediaListItem> GetPDFDocuments(List<clsOntologyItem> refItems, IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run<ResultMediaListItem>(async() =>
           {
               var result = new ResultMediaListItem
               {
                   Result = Globals.LState_Success.Clone(),
                   MediaListItems = new List<MediaListItem>()
               };

               messageOutput?.OutputInfo($"Get Pdf-Documents...");

               var mediaItemType = Config.LocalData.Class_PDF_Documents;
               var resultMediaItem = await GetMediaListItems(refItems, mediaItemType);

               result.Result = resultMediaItem.Result;
               if (result.Result.GUID == Globals.LState_Error.GUID)
               {
                   result.Result.Additional1 = "Error while getting the PDF-Files";
                   return result;
               }

               result.MediaListItems = resultMediaItem.MediaListItems;

               var mediaStoreController = new MediaStore_Module.MediaStoreConnector(Globals);

               foreach (var mediaItem in result.MediaListItems)
               {
                   mediaItem.FileUrl = mediaStoreController.GetMediaBlobPath(mediaItem.IdFile);
               }

               return result;
           });
            return taskResult;
        }

        public async Task<ResultMediaListItem> GetImages(List<clsOntologyItem> refItems, IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run<ResultMediaListItem>(async () =>
            {
                var result = new ResultMediaListItem
                {
                    Result = Globals.LState_Success.Clone(),
                    MediaListItems = new List<MediaListItem>()
                };

                messageOutput?.OutputInfo($"Get Images...");

                var mediaItemType = Config.LocalData.Class_Images__Graphic_;
                var resultMediaItem = await GetMediaListItems(refItems, mediaItemType);

                result.Result = resultMediaItem.Result;
                if (result.Result.GUID == Globals.LState_Error.GUID)
                {
                    result.Result.Additional1 = "Error while getting the Images";
                    return result;
                }

                result.MediaListItems = resultMediaItem.MediaListItems;

                var mediaStoreController = new MediaStore_Module.MediaStoreConnector(Globals);

                foreach (var mediaItem in result.MediaListItems)
                {
                    mediaItem.FileUrl = mediaStoreController.GetMediaBlobPath(mediaItem.IdFile);
                }

                return result;
            });
            return taskResult;
        }

        public async Task<ResultMediaListItem> GetMediaListItems(List<clsOntologyItem> refItems, string mediaStorePath, string mediaStoreUrl, MultimediaItemType mediaType, IMessageOutput messageOutput = null)
        {

            if (mediaType == MultimediaItemType.Unknown)
            {
                return new ResultMediaListItem
                {
                    Result = Globals.LState_Error.Clone(),
                    MediaListItems = new List<MediaListItem>()
                };
            }

            var mediaItemType = Config.LocalData.Class_Images__Graphic_;
            if (mediaType == MultimediaItemType.Audio ||
                mediaType == MultimediaItemType.Video ||
                mediaType == MultimediaItemType.VideoAndAudio)
            {
                mediaItemType = Config.LocalData.Class_Media_Item;
            }
            else if (mediaType == MultimediaItemType.PDF)
            {
                mediaItemType = Config.LocalData.Class_PDF_Documents;
            }
            messageOutput?.OutputInfo($"Media-Type: {mediaType.ToString()}");

            messageOutput?.OutputInfo($"Get media items...");
            var mediaListItems = await GetMediaListItems(refItems, mediaItemType);

            if (mediaListItems.Result.GUID == Globals.LState_Success.GUID)
            {
                messageOutput?.OutputInfo($"Have {mediaListItems.MediaListItems.Count}");
                var mediaStoreConnector = new MediaStoreConnector(Globals);
                var getMediaServiceHost = await mediaStoreConnector.GetMediaServiceHost();

                if (getMediaServiceHost.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError($"The Media-Server is not running: {getMediaServiceHost.ResultState.Additional1}");
                    return new ResultMediaListItem
                    {
                        Result = getMediaServiceHost.ResultState,
                        MediaListItems = new List<MediaListItem>()
                    };
                }

                var taskResult = await Task.Run<bool>(() =>
                {

                    var client = new RestClient(getMediaServiceHost.Result.ServiceUrl);
                    foreach (var mediaListItem in mediaListItems.MediaListItems)
                    {
                        messageOutput?.OutputInfo($"Export {mediaListItem.NameMultimediaItem} ...");
                        var path = System.IO.Path.Combine(mediaStorePath, mediaListItem.IdFile.Substring(0, 2), mediaListItem.IdFile.Substring(1, 2));
                        var filePath = System.IO.Path.Combine(path, mediaListItem.IdFile) + System.IO.Path.GetExtension(mediaListItem.NameFile);
                        var filePathThumb = System.IO.Path.Combine(path, $"thumb_{mediaListItem.IdFile}") + System.IO.Path.GetExtension(mediaListItem.NameFile);
                        var fileUrl = $"{mediaStoreUrl}/{mediaListItem.IdFile.Substring(0, 2)}/{mediaListItem.IdFile.Substring(1, 2)}/{mediaListItem.IdFile}{System.IO.Path.GetExtension(mediaListItem.NameFile)}";
                        var fileUrlThumb = $"{mediaStoreUrl}/{mediaListItem.IdFile.Substring(0, 2)}/{mediaListItem.IdFile.Substring(1, 2)}/thumb_{mediaListItem.IdFile}{System.IO.Path.GetExtension(mediaListItem.NameFile)}";
                        mediaListItem.FileUrl = fileUrl;
                        mediaListItem.ThumbFileUrl = fileUrlThumb;
                        if (!System.IO.File.Exists(filePath))
                        {
                            var request = new RestRequest("copyToMediaStore", Method.POST);
                            request.AddParameter("idFile", mediaListItem.IdFile); // adds to POST or URL querystring based on Method
                            request.AddParameter("PathDst", path);

                            IRestResponse<MediaItemPathMap> response2 = client.Execute<MediaItemPathMap>(request);
                            if (!response2.IsSuccessful)
                            {
                                messageOutput?.OutputError($"The Mediaitem cannot be exported: {response2.ErrorMessage}");
                            }
                            else
                            {
                                messageOutput?.OutputInfo($"Exported {mediaListItem.NameMultimediaItem}");
                            }
                        }
                        else
                        {
                            messageOutput?.OutputInfo($"Exported {mediaListItem.NameMultimediaItem}");
                        }

                        if (mediaType == MultimediaItemType.Image && System.IO.File.Exists(filePath) && !System.IO.File.Exists(filePathThumb))
                        {
                            messageOutput?.OutputInfo($"Export Thumbnail ...");
                            try
                            {
                                var thumbNailImage = new ThumbNailImage();
                                var image = Image.FromFile(filePath);
                                var thumbImage = thumbNailImage.GetThumbNailOfImageByMax(image, 100);
                                thumbImage.Save(filePathThumb);
                                thumbImage.Dispose();
                                image.Dispose();
                                messageOutput?.OutputInfo($"Exported Thumbnail.");
                            }
                            catch (Exception ex)
                            {
                                messageOutput?.OutputError($"The Thumbnail cannot be exported: {ex.Message}");
                            }

                        }


                    }
                    return true;
                });
            }

            return mediaListItems;
        }

        public ResultItem<OItemWithParent> GetOItem(string id, string type)
        {
            var result = new ResultItem<OItemWithParent>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new OItemWithParent()
            };

            var oItem = elasticAgentService.GetOItem(id, type).Clone();
            if (oItem == null || oItem.GUID_Related == Globals.LState_Error.GUID)
            {
                result.ResultState = Globals.LState_Error.Clone();
                return result;
            }

            result.Result.OItem = oItem;
            result.Result.OItemParent = null;

            if (type == Globals.Type_Object)
            {
                var oItemParent = elasticAgentService.GetOItem(oItem.GUID_Parent, Globals.Type_Class);
                if (oItemParent == null || oItemParent.GUID_Related == Globals.LState_Error.GUID)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    return result;
                }

                result.Result.OItemParent = oItemParent;
            }

            return result;
        }


        public async Task<ResultMediaListItem> SaveFileItems(clsOntologyItem refItem, List<clsOntologyItem> fileItems, string type)
        {
            var result = new ResultMediaListItem
            {
                Result = Globals.LState_Success.Clone(),

            };

            var multimediaItems = await elasticAgentService.SaveFileItems(refItem, fileItems, type);

            if (multimediaItems.Result.GUID == Globals.LState_Error.GUID)
            {
                result.Result = multimediaItems.Result;
                return result;
            }

            var relationConfig = new clsRelationConfig(Globals);
            multimediaItems.FilesToCreateDate = fileItems.Where(fileItem => fileItem.Val_Date != null).Select(fileItem => relationConfig.Rel_ObjectAttribute(fileItem, Config.LocalData.AttributeType_Datetimestamp__Create_, fileItem.Val_Date, orderId: 1)).ToList();

            var mediaListFactory = new MediaItemListFactory(Globals);

            var mediaItemList = await mediaListFactory.GetMediaItemList(multimediaItems);

            return mediaItemList;
        }

        private async Task<ResultMediaListItem> GetMediaListItems(List<clsOntologyItem> refItems, clsOntologyItem mediaItemType)
        {
            var result = new ResultMediaListItem
            {
                Result = Globals.LState_Success.Clone()
            };

            var resultService = await elasticAgentService.SearchMediaItems(refItems, mediaItemType);

            if (resultService.Result.GUID == Globals.LState_Error.GUID)
            {
                result.Result = resultService.Result;
                return result;
            }

            var resultFactory = await factory.GetMediaItemList(resultService);

            if (resultFactory.Result.GUID == Globals.LState_Error.GUID)
            {
                result.Result = resultFactory.Result;
                return result;
            }

            MediaListItems = result;
            result.MediaListItems = resultFactory.MediaListItems;
            return result;

        }

        public async Task<clsOntologyItem> ChangeOrderIdAndName(List<MediaListItem> mediaListItems)
        {
            var serviceAgent = new ElasticAgentService(Globals);

            var result = Globals.LState_Success.Clone();

            if (!mediaListItems.Any())
            {
                result = Globals.LState_Nothing.Clone();
                result.Additional1 = "No MediaItems provided!";
                return result;
            }

            var searchRelation = mediaListItems.Select(mediaListItem => new clsObjectRel
            {
                ID_Object = mediaListItem.IdMultimediaItem,
                ID_Other = mediaListItem.IdRef,
                ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID
            }).ToList();


            var controllerResult = await serviceAgent.GetMediaItemsToRefRelations(searchRelation);

            result = controllerResult.ResultState;

            if (result.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            if (!controllerResult.Result.Any())
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = "No relation between MediaItem and Reference found!";
                return result;
            }

            var differentMediaItems = from mediaListItem in mediaListItems
                                      join rel in controllerResult.Result on new { ID_Object = mediaListItem.IdMultimediaItem, ID_Other = mediaListItem.IdRef } equals new { rel.ID_Object, rel.ID_Other }
                                      where mediaListItem.OrderId != rel.OrderID || mediaListItem.NameMultimediaItem != rel.Name_Object
                                      select new { mediaListItem, rel, DiffOrderId = mediaListItem.OrderId != rel.OrderID, DiffName = mediaListItem.NameMultimediaItem != rel.Name_Object };

            var differentOrderIds = new List<clsObjectRel>();
            var differentNames = new List<clsOntologyItem>();

            differentMediaItems.Where(rel => rel.DiffOrderId).ToList().ForEach(rel =>
            {
                rel.rel.OrderID = rel.mediaListItem.OrderId;
                differentOrderIds.Add(rel.rel.Clone());
            });

            differentMediaItems.Where(rel => rel.DiffName).ToList().ForEach(rel =>
            {
                rel.rel.Name_Object = rel.mediaListItem.NameMultimediaItem;
                differentNames.Add(new clsOntologyItem { GUID = rel.rel.ID_Object, Name = rel.rel.Name_Object, GUID_Parent = Config.LocalData.Class_Media_Item.GUID, Type = Globals.Type_Object });

            });

            result = await serviceAgent.UpdateMediaListItems(differentOrderIds, differentNames);


            return result;
        }

        public string ConvertOMediaTypeToIdClassMediaType(MultimediaItemType mediaType)
        {
            if (mediaType == MultimediaItemType.Audio ||
                mediaType == MultimediaItemType.Video)
            {
                return Config.LocalData.Class_Media_Item.GUID;
            }
            else if (mediaType == MultimediaItemType.Image)
            {
                return Config.LocalData.Class_Images__Graphic_.GUID;
            }
            else if (mediaType == MultimediaItemType.PDF)
            {
                return Config.LocalData.Class_PDF_Documents.GUID;
            }

            return null;
        }

        public async Task<ResultItem<AssociateMediaItemsResult>> AssociateFilesToMediaItems(AssociateMediaItemsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<AssociateMediaItemsResult>>(async () =>
            {
                var result = new ResultItem<AssociateMediaItemsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new AssociateMediaItemsResult()
                };

                if (!request.IsValid)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Request is not valid: {request.ValidationMessage}";
                }

                var elasticAgent = new ElasticAgentService(Globals);


                var searchItems = request.Filters.Select(filterItem =>
                {
                    var resultItem = new clsOntologyItem();

                    resultItem.GUID_Parent = MediaViewerModule.Connectors.MediaViewerConnector.ClassFile.GUID;
                    if (filterItem.RegexName)
                    {
                        resultItem.Name = null;

                    }

                    return resultItem;
                }).ToList();

                var serviceResultFiles = await elasticAgent.GetFileItems(searchItems);

                result.ResultState = serviceResultFiles.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var filesToRelate = new List<clsOntologyItem>();
                var filtersWithNames = request.Filters.Where(filter => !filter.RegexName && !string.IsNullOrEmpty(filter.FileItem.Name));
                if (filtersWithNames.Any())
                {
                    filesToRelate.AddRange(from fileInDb in serviceResultFiles.Result
                                           join fileItem in filtersWithNames on fileInDb.Name equals fileItem.FileItem.Name
                                           select fileInDb);
                }

                if (request.RegexFilter != null)
                {
                    filesToRelate.AddRange(serviceResultFiles.Result.Where(obj => request.RegexFilter.Match(obj.Name).Success));
                }


                var serviceResultRel = await elasticAgent.GetMultimediaItemsOfFiles(filesToRelate, request.TypeOfAssociation);

                result.ResultState = serviceResultRel.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                filesToRelate = (from file in filesToRelate
                                 join related in serviceResultRel.Result on file.GUID equals related.ID_Other into relateds
                                 from related in relateds.DefaultIfEmpty()
                                 where related == null
                                 select file).ToList();

                var serviceResultSaveRel = await elasticAgent.SaveNewMediaItems(filesToRelate, request.TypeOfAssociation);

                result.ResultState = serviceResultSaveRel.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.AssociatedFiles = filesToRelate;
                result.Result.AssociatedMediaItems = serviceResultSaveRel.Result.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Object,
                    Name = rel.Name_Object,
                    GUID_Parent = rel.ID_Parent_Object,
                    Type = Globals.Type_Object
                }).ToList();

                if (request.RefItem != null)
                {
                    var serviceResultRelRef = await elasticAgent.RelateMediaItems(result.Result.AssociatedMediaItems, request.RefItem, true);
                    result.ResultState = serviceResultRelRef.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> CheckMediaServier()
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var mediaStoreConnector = new MediaStoreConnector(Globals);
                var getMediaStoreHostResult = await mediaStoreConnector.GetMediaServiceHost();
                if (getMediaStoreHostResult.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return getMediaStoreHostResult.ResultState;
                }

                var restClient = new RestClient(getMediaStoreHostResult.Result.ServiceUrl);
                var restRequest = new RestRequest("checkMediaServer", Method.GET);

                IRestResponse<bool> response = restClient.Execute<bool>(restRequest);

                var result = response.Data ? Globals.LState_Success.Clone() : Globals.LState_Error.Clone();
                result.Additional1 = "Mediaserver is not running!";
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<RelateFilesWithMediaItemsAndRefResult>> RelateFilesWithMediaItemsAndRef(RelateFilesWithMediaItemsAndRefRequest request)
        {
            var taskResult = await Task.Run<ResultItem<RelateFilesWithMediaItemsAndRefResult>>(async () =>
           {
               var result = new ResultItem<RelateFilesWithMediaItemsAndRefResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new RelateFilesWithMediaItemsAndRefResult()
               };

               var elasticAgent = new ElasticAgentService(Globals);

               var getRefItemResult = elasticAgent.GetOItem(request.RefItem.GUID, Globals.Type_Object);

               if (getRefItemResult.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Error while getting the RefItem!";
                   return result;
               }

               request.RefItem = getRefItemResult;

               var getFileListResult = await elasticAgent.GetObjects(request.FileItems.Select(fileItem => fileItem.GUID).ToList());

               result.ResultState = getFileListResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               request.FileItems = getFileListResult.Result;

               var type = "";
               switch (request.MediaType)
               {
                   case MultimediaItemType.Audio:
                       type = Config.LocalData.Class_Media_Item.GUID;
                       break;
                   case MultimediaItemType.Image:
                       type = Config.LocalData.Class_Images__Graphic_.GUID;
                       break;
                   case MultimediaItemType.PDF:
                       type = Config.LocalData.Class_PDF_Documents.GUID;
                       break;
                   case MultimediaItemType.Video:
                       type = Config.LocalData.Class_Media_Item.GUID;
                       break;
                   default:
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "The provided MediaItemType is not valid!";
                       return result;

               }
               var saveFileItemsResult = await SaveFileItems(request.RefItem, request.FileItems, type);

               result.ResultState = saveFileItemsResult.Result;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               result.Result.RefItem = request.RefItem;
               result.Result.FileItems = request.FileItems;
               result.Result.MediaItems = saveFileItemsResult.MediaListItems;

               return result;
           });

            return taskResult;
        }


        public MediaViewerConnector(Globals globals) : base(globals)
        {
            Initialize();
        }

        private void Initialize()
        {
            elasticAgentService = new ElasticAgentService(Globals);
            factory = new MediaItemListFactory(Globals);
        }

    }
}
