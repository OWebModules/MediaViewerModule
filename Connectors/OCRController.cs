﻿using ElasticSearchNestConnector;
using MediaViewerModule.Models;
using MediaViewerModule.Services;
using MediaViewerModule.Validation;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesseract;

namespace MediaViewerModule.Connectors
{
    public class OCRController : AppController
    {

        public async Task<ResultItem<OCRImagesResult>>  OCRImages(OCRImagesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<OCRImagesResult>>(async() =>
            {
                var result = new ResultItem<OCRImagesResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new OCRImagesResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");
                result.ResultState = ValidationController.ValidateOCRImagesRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Validated request.");

                var elasticAgent = new ElasticAgentService(Globals);

                if (request.CancellationToken != null && request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState.Additional1 = "Cancelled by User!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Get Model...");
                var getModelResult = await elasticAgent.GetOCRImagesModel(request);
                result.ResultState = getModelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Model...");

                if (request.CancellationToken != null && request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState.Additional1 = "Cancelled by User!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (getModelResult.Result.ConfigToPath != null)
                {

                }

                var imagessToIndexPre = (from imagesFile in getModelResult.Result.Images
                                      join indexedFile in getModelResult.Result.SessionOCRs on imagesFile.IdMultimediaItem equals indexedFile.ID_Other into indexedFiles
                                      from indexedFile in indexedFiles.DefaultIfEmpty()
                                      where indexedFile == null
                                      select imagesFile).ToList();

                var imagessToIndex = (from imagesFile in imagessToIndexPre
                                   join indexedFileNotExisting in getModelResult.Result.SessionOCRsNotExisting on imagesFile.IdMultimediaItem equals indexedFileNotExisting.ID_Other into notExistings
                                   from indexedFileNotExisting in notExistings.DefaultIfEmpty()
                                   where indexedFileNotExisting == null
                                   select imagesFile).ToList();

                imagessToIndex = (from imagesFile in imagessToIndex
                               join indexedFileError in getModelResult.Result.SessionOCRsError on imagesFile.IdMultimediaItem equals indexedFileError.ID_Other into indexedFileErrors
                               from indexedFileError in indexedFileErrors.DefaultIfEmpty()
                               where indexedFileError == null
                               select imagesFile).ToList();

                if (request.CancellationToken != null && request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState.Additional1 = "Cancelled by User!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var indexCount = imagessToIndex.Count();
                var checkedCount = 0;
                var savedCount = 0;
                request.MessageOutput?.OutputInfo($"Documents to index: {indexCount}");

                var stampStart = DateTime.Now;
                var relationConfig = new clsRelationConfig(Globals);

                var sessionItem = new clsOntologyItem
                {
                    GUID = Globals.NewGUID,
                    Name = stampStart.ToString("yyyy-MM-dd hh:mm:ss"),
                    GUID_Parent = OCR.Config.LocalData.Class_Index_Session__OCR_.GUID,
                    Type = Globals.Type_Object
                };

                var objectsToSave = new List<clsOntologyItem>
               {
                   sessionItem
               };

                var attributesToSave = new List<clsObjectAtt>();

                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(sessionItem, OCR.Config.LocalData.AttributeType_Datetimestamp__Create_, stampStart));

                request.MessageOutput?.OutputInfo($"Save Session-Item: {sessionItem.Name}");
                result.ResultState = await elasticAgent.SaveObjects(objectsToSave);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the objects!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Save create stamp");
                result.ResultState = await elasticAgent.SaveAttributes(attributesToSave);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the attributes!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var relationsToSave = new List<clsObjectRel>();
                var relationsNotExisting = new List<clsObjectRel>();
                var relationsError = new List<clsObjectRel>();

                var dbReader = new clsUserAppDBSelector(getModelResult.Result.ElasticSearchConfig.NameServer, getModelResult.Result.ElasticSearchConfig.Port, getModelResult.Result.ElasticSearchConfig.NameIndex, 5000, Globals.Session);
                var dbWriter = new clsUserAppDBUpdater(dbReader);
                var documents = new List<clsAppDocuments>();

                foreach (var imageFile in imagessToIndex)
                {
                    if (request.CancellationToken != null && request.CancellationToken.IsCancellationRequested)
                    {
                        result.ResultState.Additional1 = "Cancelled by User!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    checkedCount++;
                    var resultItem = new ResultItem<clsOntologyItem>
                    {
                        ResultState = Globals.LState_Success.Clone(),
                        Result = new clsOntologyItem
                        {
                            GUID = imageFile.IdMultimediaItem,
                            Name = imageFile.NameMultimediaItem,
                            GUID_Parent = OCR.Config.LocalData.Class_Images__Graphic_.GUID,
                            Type = Globals.Type_Object
                        }
                    };

                    result.Result.OCRResult.Add(resultItem);

                    if (!System.IO.File.Exists(imageFile.FileUrl))
                    {
                        resultItem.ResultState = Globals.LState_Error.Clone();
                        resultItem.ResultState.Additional1 = $"The File {imageFile.FileUrl} does not exist!";
                        relationsNotExisting.Add(relationConfig.Rel_ObjectRelation(sessionItem, new clsOntologyItem
                        {
                            GUID = imageFile.IdMultimediaItem,
                            Name = imageFile.NameMultimediaItem,
                            GUID_Parent = OCR.Config.LocalData.Class_Images__Graphic_.GUID,
                            Type = Globals.Type_Object
                        }, OCR.Config.LocalData.RelationType_not_existing));
                        continue;
                    }

                    try
                    {


                        using (var engine = new Tesseract.TesseractEngine("./testdata", "eng", Tesseract.EngineMode.Default))
                        {
                            using (var img = Pix.LoadFromFile(imageFile.FileUrl))
                            {
                                using (var page = engine.Process(img))
                                {
                                    var text = page.GetText();
                                    var document = new clsAppDocuments
                                    {
                                        Id = $"{imageFile.IdMultimediaItem}",
                                        Dict = new Dictionary<string, object>()
                                    };
                                    documents.Add(document);
                                    document.Dict.Add(OCRIndex.FieldId, imageFile.IdMultimediaItem);
                                    document.Dict.Add(OCRIndex.FieldContent, text);
                                }
                            }
                        }

                        relationsToSave.Add(relationConfig.Rel_ObjectRelation(sessionItem, new clsOntologyItem
                        {
                            GUID = imageFile.IdMultimediaItem,
                            Name = imageFile.NameMultimediaItem,
                            GUID_Parent = OCR.Config.LocalData.Class_Images__Graphic_.GUID,
                            Type = Globals.Type_Object
                        }, OCR.Config.LocalData.RelationType_contains));

                        if (documents.Count % 20 == 0)
                        {
                            request.MessageOutput?.OutputInfo($"Checked {checkedCount}/{indexCount}");
                        }
                        if (documents.Count >= 500)
                        {
                            savedCount += documents.Count;

                            result.ResultState = dbWriter.SaveDoc(documents, getModelResult.Result.ElasticSearchConfig.NameType);
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                result.ResultState.Additional1 = "Error saving documents!";
                                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                            }
                            documents.Clear();

                            result.ResultState = await elasticAgent.SaveRelations(relationsToSave);
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                result.ResultState.Additional1 = "Error while saving the relations!";
                                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                            }

                            relationsToSave.Clear();
                            request.MessageOutput?.OutputInfo($"Checked {checkedCount}/{indexCount}, Saved { savedCount}");
                        }


                    }
                    catch (Exception ex)
                    {
                        resultItem.Result = Globals.LState_Error.Clone();
                        resultItem.Result.Additional1 = ex.Message;
                        relationsError.Add(relationConfig.Rel_ObjectRelation(sessionItem, new clsOntologyItem
                        {
                            GUID = imageFile.IdMultimediaItem,
                            Name = imageFile.NameMultimediaItem,
                            GUID_Parent = OCR.Config.LocalData.Class_Images__Graphic_.GUID,
                            Type = Globals.Type_Object
                        }, OCR.Config.LocalData.RelationType_error));
                    }
                }

                if (documents.Any())
                {
                    savedCount += documents.Count;
                    result.ResultState = dbWriter.SaveDoc(documents, getModelResult.Result.ElasticSearchConfig.NameType);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error saving documents!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    documents.Clear();
                    request.MessageOutput?.OutputInfo($"Checked {checkedCount}/{indexCount}, Saved { savedCount}");
                }

                if (relationsToSave.Any())
                {
                    request.MessageOutput?.OutputInfo($"Save last {relationsToSave.Count} relations.");
                    result.ResultState = await elasticAgent.SaveRelations(relationsToSave);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the relations!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    relationsToSave.Clear();
                }

                if (relationsNotExisting.Any())
                {
                    request.MessageOutput?.OutputInfo($"Save last {relationsNotExisting.Count} not existing.");
                    result.ResultState = await elasticAgent.SaveRelations(relationsNotExisting);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the not existing documents!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    relationsNotExisting.Clear();
                }

                if (relationsError.Any())
                {
                    request.MessageOutput?.OutputInfo($"Save last {relationsError.Count} errors.");
                    result.ResultState = await elasticAgent.SaveRelations(relationsError);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the error-documents!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    relationsError.Clear();
                }

                var stampFinished = DateTime.Now;

                attributesToSave = new List<clsObjectAtt>();
                request.MessageOutput?.OutputInfo($"Save finishing: {stampFinished}.");
                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(sessionItem, OCR.Config.LocalData.AttributeType_Finished, stampFinished));

                result.ResultState = await elasticAgent.SaveAttributes(attributesToSave);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the finishing stamp!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public OCRController(Globals globals) : base(globals)
        {
        }
    }
}
