﻿using MediaStore_Module;
using MediaViewerModule.Factories;
using MediaViewerModule.Models;
using MediaViewerModule.Services;
using MediaViewerModule.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Connectors
{
    public class ImageMapController : AppController
    {
        public async Task<clsOntologyItem> CheckMediaServier()
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var mediaStoreConnector = new MediaStoreConnector(Globals);
                var getMediaStoreHostResult = await mediaStoreConnector.GetMediaServiceHost();
                if (getMediaStoreHostResult.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return getMediaStoreHostResult.ResultState;
                }

                var restClient = new RestClient(getMediaStoreHostResult.Result.ServiceUrl);
                var restRequest = new RestRequest("checkMediaServer", Method.GET);

                IRestResponse<bool> response = restClient.Execute<bool>(restRequest);

                var result = response.Data ? Globals.LState_Success.Clone() : Globals.LState_Error.Clone();
                result.Additional1 = "Mediaserver is not running!";
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<Models.ImageMap>> GetImageMap(GetImageMapRequest request)
        {
            var taskResult = await Task.Run<ResultItem<Models.ImageMap>>(async() =>
            {
                var result = new ResultItem<Models.ImageMap>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                request.MessageOutput?.OutputInfo("Validate request...");
                result.ResultState = ValidationController.ValidateGetImageMapRequest(request, Globals);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Validated request.");

                request.MessageOutput?.OutputInfo("Get model...");
                var elasticAgent = new Services.ElasticAgentService(Globals);

                var modelResult = await elasticAgent.GetImageMapModel(request);
                result.ResultState = modelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have model.");

                request.MessageOutput?.OutputInfo("Create ImageMap...");

                var factory = new ImageMapFactory(Globals);
                var factoryResult = await factory.CreateImageMap(modelResult.Result);

                result.ResultState = factoryResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have ImageMap.");

                result.Result = factoryResult.Result;

                var mediaViewerController = new MediaViewerConnector(Globals);
                var mediaItemsListResult = await mediaViewerController.GetMediaListItems(new List<OntologyClasses.BaseClasses.clsOntologyItem> { modelResult.Result.ImageMap }, request.MediaStorePath, request.MediaStoreUrl, Primitives.MultimediaItemType.Image, request.MessageOutput);
                result.ResultState = mediaItemsListResult.Result;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var imageItem = mediaItemsListResult.MediaListItems.FirstOrDefault();

                if (imageItem == null)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Image found!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                result.Result.ImageUrl = imageItem.FileUrl;
                

                return result;
            });

            return taskResult;
        }

        public ImageMapController(Globals globals) : base(globals)
        {
        }
    }
}
