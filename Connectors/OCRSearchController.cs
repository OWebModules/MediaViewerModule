﻿using MediaViewerModule.Factories;
using MediaViewerModule.Models;
using Nest;
using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Connectors
{
    public class OCRSearchController : AppController
    {

        public async Task<ResultItem<SearchOCRItemsResult>> SearchOCRItems(SearchOCRItemsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<SearchOCRItemsResult>>(async () =>
            {
                var result = new ResultItem<SearchOCRItemsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new SearchOCRItemsResult()
                };


                var elasticSearchAgent = new Services.ElasticAgentService(Globals);
                var elasticSearchConfig = new ElasticSearchConfigModule.ElasticSearchConfigController(Globals);

                request.MessageOutput?.OutputInfo("Get configuration...");
                var configResult = await elasticSearchConfig.GetConfig(OCR.Config.LocalData.Object_OCRDefault, OCR.Config.LocalData.RelationType_belonging_Destination, OCR.Config.LocalData.RelationType_uses, Globals.Direction_LeftRight);

                result.ResultState = configResult.ResultState;

                if (configResult.Result == null)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Elasticsearch-Index or -Type found!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Have configuration.");

                var dbReader = new ElasticSearchNestConnector.clsUserAppDBSelector(configResult.Result.NameServer, configResult.Result.Port, configResult.Result.NameIndex, 5000, Globals.Session);

                var query = $"Content:{ (string.IsNullOrEmpty(request.SearchString) ? " * " : request.SearchString)}";
                var sortFields = request.SortRequests.Select(sort => new SortField { Field = sort.field, Order = sort.dir == "asc" ? SortOrder.Ascending : SortOrder.Descending }).ToList();

                request.MessageOutput?.OutputInfo("Search pdf raw documents...");
                var searchResult = dbReader.GetData_Documents(request.PageSize, configResult.Result.NameIndex, configResult.Result.NameType, query, request.Page, request.ScrollId, sortFields);

                if (!searchResult.IsOK)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = searchResult.ErrorMessage;
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var docs = searchResult.Documents.GroupBy(docItm => new
                {
                    ImageId = docItm.Dict["ImageId"].ToString(),
                    Content = docItm.Dict["Content"].ToString()
                }).Select(docGrp => new OCRIdAndContent
                {
                    Id = docGrp.Key.ImageId,
                    Content = docGrp.Key.Content
                }).ToList();

                request.MessageOutput?.OutputInfo($"Have raw ocr documents: {docs.Count}");

                request.MessageOutput?.OutputInfo("Search references...");
                var refResult = await elasticSearchAgent.GetOCRRawItems(docs);

                result.ResultState = refResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Have references.");

                request.MessageOutput?.OutputInfo("Create result...");
                var createListResult = await OCRSearchGridViewItemFactory.CreateGridViewItemList(refResult.Result, docs, Globals, request.SearchString);

                result.ResultState = createListResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Have result.");

                result.Result = new SearchOCRItemsResult
                {
                    ScrollId = searchResult.ScrollId,
                    GridItems = createListResult.Result,
                    TotalCount = searchResult.TotalCount
                };

                result.Result.ScrollId = searchResult.ScrollId;

                return result;
            });

            return taskResult;
        }

        public OCRSearchController(Globals globals) : base(globals)
        {
        }
    }
}
