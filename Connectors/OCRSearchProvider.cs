﻿using MediaViewerModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Connectors
{
    public class OCRSearchProvider : ISearchProvider
    {
        private Globals globals;
        public string ProviderName => "OCR Search";

        public string ProviderDescription => "Searches OCR documents";

        public string ProviderId { get; private set; }

        public bool ActionInAdditional2 => false;

        public async Task<ResultItem<List<clsOntologyItem>>> Search(string searchString, IMessageOutput messageOutput = null)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsOntologyItem>()
               };

               var pdfSearchController = new OCRSearchController(globals);
               var searchRequest = new SearchOCRItemsRequest(searchString) { MessageOutput = messageOutput };
               var searchResult = await pdfSearchController.SearchOCRItems(searchRequest);
               result.ResultState = searchResult.ResultState;

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               result.Result = searchResult.Result.GridItems.Select(itm => new clsOntologyItem
               {
                   GUID = itm.IdOCRDocument,
                   Name = itm.NameOCRDocument,
                   GUID_Parent = Config.LocalData.Class_PDF_Documents.GUID,
                   Additional1 = itm.Content
               }).ToList();

               return result;
           });

            return taskResult;
        }

        public OCRSearchProvider(Globals globals)
        {
            ProviderId = Guid.NewGuid().ToString();
            this.globals = globals;
        }

        public OCRSearchProvider()
        {
        }
    }
}
