﻿using MediaViewerModule.Models;
using Nest;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MediaViewerModule.Connectors
{
    public class PDFNameTransformController : INameTransform
    {
        private Globals globals;

        public bool IsReferenceCompatible => false;

        public bool IsAspNetProject { get; set; }

        public async Task<ResultItem<List<clsOntologyItem>>> TransformNames(List<clsOntologyItem> items, bool encodeName = true, string idClass = null, IMessageOutput messageOutput = null)
        {
            var taskResult = await Task.Run(async () =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = items
               };

               messageOutput?.OutputInfo("Validate items...");

               if (!items.Any())
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No items provided!";
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               if (!items.All(itm => itm.GUID_Parent == Config.LocalData.Class_PDF_Documents.GUID))
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = $"Not all items are of class {Config.LocalData.Class_PDF_Documents.Name}!";
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;

               }

               messageOutput?.OutputInfo("Validated items.");

               var elasticSearchAgent = new Services.ElasticAgentService(globals);
               var elasticSearchConfig = new ElasticSearchConfigModule.ElasticSearchConfigController(globals);

               messageOutput?.OutputInfo("Get configuration...");
               var configResult = await elasticSearchConfig.GetConfig(SearchPdf.Config.LocalData.Object_Default, SearchPdf.Config.LocalData.RelationType_belonging_Destination, SearchPdf.Config.LocalData.RelationType_uses, globals.Direction_LeftRight);

               result.ResultState = configResult.ResultState;

               if (configResult.Result == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Elasticsearch-Index or -Type found!";
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               messageOutput?.OutputInfo("Have configuration.");

               var dbReader = new ElasticSearchNestConnector.clsUserAppDBSelector(configResult.Result.NameServer, configResult.Result.Port, configResult.Result.NameIndex, 5000, globals.Session);

               foreach (var item in items)
               {
                   var query = $"PdfId:{item.GUID}";
                   var sortFields = new List<SortField> { new SortField { Field = "Page", Order = SortOrder.Ascending } };

                   messageOutput?.OutputInfo("Search pdf raw documents...");
                   var searchResult = dbReader.GetData_Documents(-1, configResult.Result.NameIndex, configResult.Result.NameType, query, sortFields: sortFields);

                   if (!searchResult.IsOK)
                   {
                       result.ResultState = globals.LState_Error.Clone();
                       result.ResultState.Additional1 = searchResult.ErrorMessage;
                       messageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   var docs = searchResult.Documents.GroupBy(docItm => new
                   {
                       PdfId = docItm.Dict["PdfId"].ToString(),
                       Content = docItm.Dict["Content"].ToString(),
                       Page = (long)docItm.Dict["Page"]
                   }).Select(docGrp => new PDFIdAndContent
                   {
                       Id = docGrp.Key.PdfId,
                       Content = docGrp.Key.Content,
                       Page = docGrp.Key.Page
                   }).ToList();

                   messageOutput?.OutputInfo($"Have raw pdf documents: {docs.Count}");

                   var sbContent = new StringBuilder();
                   docs.ForEach(doc =>
                   {
                       sbContent.AppendLine("<p>" + doc.Content.Replace("\n", "<br>") + "</p>");
                   });

                   if (encodeName)
                   {
                       item.Val_String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(sbContent.ToString()));
                   }
                   else
                   {
                       item.Name = sbContent.ToString();
                   }
               }
               

               return result;
           });

            return taskResult;
        }

        public bool IsResponsible(string idClass)
        {
            return idClass == Config.LocalData.Class_PDF_Documents.GUID;
        }

        public PDFNameTransformController(Globals globals)
        {
            this.globals = globals;
        }
    }
}
