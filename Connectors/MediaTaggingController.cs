﻿using MediaStore_Module;
using MediaStore_Module.Models;
using MediaViewerModule.Factories;
using MediaViewerModule.Models;
using MediaViewerModule.Primitives;
using MediaViewerModule.Services;
using MediaViewerModule.Validation;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MediaViewerModule.Connectors
{
    public class MediaTaggingController : AppController
    {
        private MediaStoreConnector mediaStoreConnector;
        public clsOntologyItem LogStatePosition
        {
            get
            {
                return Config.LocalData.Object_Position;
            }
        }

        public async Task<clsOntologyItem> CheckMediaServier()
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var mediaStoreConnector = new MediaStoreConnector(Globals);
                var getMediaStoreHostResult = await mediaStoreConnector.GetMediaServiceHost();
                if (getMediaStoreHostResult.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return getMediaStoreHostResult.ResultState;
                }

                var restClient = new RestClient(getMediaStoreHostResult.Result.ServiceUrl);
                var restRequest = new RestRequest("checkMediaServer", Method.GET);

                IRestResponse<bool> response = restClient.Execute<bool>(restRequest);

                var result = response.Data ? Globals.LState_Success.Clone() : Globals.LState_Error.Clone();
                result.Additional1 = "Mediaserver is not running!";
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<TagMediaItemsResult>> TagMediaItems(TagMediaItemsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<TagMediaItemsResult>>(async () =>
            {
                request.MessageOutput?.OutputInfo($"Start tagging");
                var result = new ResultItem<TagMediaItemsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new TagMediaItemsResult()
                };

                Regex regex = null;

                if (!string.IsNullOrEmpty(request.NameRegex))
                {
                    try
                    {
                        request.MessageOutput?.OutputInfo($"Create regex-object: {request.NameRegex}");
                        regex = new Regex(request.NameRegex);
                    }
                    catch (Exception ex)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"No valid Regexfilter: {ex.Message}";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }

                if (string.IsNullOrEmpty(request.MediaStorePath))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No valid MediaStorePath";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (!System.IO.Directory.Exists(request.MediaStorePath))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "MediaStorePath does not exist!";
                    return result;
                }


                var serviceAgent = new ElasticAgentService(Globals);

                request.MessageOutput?.OutputInfo($"Get Mediaitems...");
                var serviceResult = await serviceAgent.GetMultimediaItems(request.MediaItemType, request.ItemFilter, request.Related);


                result.ResultState = serviceResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Have Mediaitems: {serviceResult.Result.MediaItems.Count}");

                var mediaItemList = serviceResult.Result.MediaItems;

                if (regex != null)
                {
                    mediaItemList = mediaItemList.Where(mediaItem => regex.Match(mediaItem.Name).Success).ToList();
                }

                request.MessageOutput?.OutputInfo($"Filtered Mediaitems: {mediaItemList.Count}");

                var mediaItemFiles = (from mediaItem in mediaItemList
                                      join fileItem in serviceResult.Result.MediaItemsToFiles on mediaItem.GUID equals fileItem.ID_Object
                                      select new TaggedMediaItem
                                      {
                                          MediaItem = mediaItem,
                                          FileItem = new clsOntologyItem
                                          {
                                              GUID = fileItem.ID_Other,
                                              Name = fileItem.Name_Other,
                                              GUID_Parent = fileItem.ID_Parent_Other,
                                              Type = Globals.Type_Object
                                          }
                                      }).ToList();


                request.MessageOutput?.OutputInfo($"Check Mediastore...");
                var getMediaServiceHost = await mediaStoreConnector.GetMediaServiceHost();
                request.MessageOutput?.OutputInfo($"Mediastore Present");

                result.ResultState = getMediaServiceHost.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var client = new RestClient(getMediaServiceHost.Result.ServiceUrl);
                try
                {
                    var errorFiles = new List<string>();
                    for (var i = 0; i < mediaItemFiles.Count; i++)
                    {
                        var mediaItem = mediaItemFiles[i];
                        if (i % 20 == 0)
                        {
                            request.MessageOutput?.OutputInfo($"Found Tags of {i} Files");
                        }
                        var path = System.IO.Path.Combine(request.MediaStorePath, mediaItem.FileItem.GUID.Substring(0, 2), mediaItem.FileItem.GUID.Substring(1, 2));
                        var filePath = System.IO.Path.Combine(path, mediaItem.FileItem.GUID) + System.IO.Path.GetExtension(mediaItem.FileItem.Name);
                        var filePathThumb = System.IO.Path.Combine(path, $"thumb_{mediaItem.FileItem.GUID}") + System.IO.Path.GetExtension(mediaItem.FileItem.Name);

                        if (!System.IO.File.Exists(filePath))
                        {
                            var restRequest = new RestRequest("copyToMediaStore", Method.POST);
                            restRequest.AddParameter("idFile", mediaItem.FileItem.GUID); // adds to POST or URL querystring based on Method
                            restRequest.AddParameter("PathDst", path);

                            IRestResponse<MediaItemPathMap> response2 = client.Execute<MediaItemPathMap>(restRequest);
                            mediaItem.MediaItem.Additional1 = filePath;
                            if (!response2.IsSuccessful)
                            {
                                errorFiles.Add(mediaItem.FileItem.Name);
                            }

                        }

                        if (System.IO.File.Exists(filePath))
                        {
                            var tfile = TagLib.File.Create(filePath);
                            mediaItem.Tag = tfile.Tag;
                        }


                    }

                    foreach (var errorFile in errorFiles)
                    {
                        request.MessageOutput?.OutputWarning($"File cannot be found: {errorFile}");
                    }

                    var years = mediaItemFiles.GroupBy(file => file.Tag.Year).Select(file => file.Key.ToString()).Where(year => year != "0").Select(year => new clsOntologyItem { Name = year, GUID_Parent = Config.LocalData.Class_Year.GUID, Type = Globals.Type_Object }).ToList();
                    var albums = mediaItemFiles.GroupBy(album => album.Tag.Album).Select(album => album.Key).Where(album => !string.IsNullOrEmpty(album)).Select(album => new clsOntologyItem { Name = album, GUID_Parent = Config.LocalData.Class_Album.GUID, Type = Globals.Type_Object }).ToList();
                    var genres = mediaItemFiles.SelectMany(genre => genre.Tag.Genres).GroupBy(genre => genre).Select(genre => genre.Key).Where(genre => !string.IsNullOrEmpty(genre)).Select(genre => new clsOntologyItem { Name = genre, GUID_Parent = Config.LocalData.Class_Genre.GUID, Type = Globals.Type_Object }).ToList();
                    var artists = mediaItemFiles.SelectMany(genre => genre.Tag.AlbumArtists).GroupBy(genre => genre).Select(genre => genre.Key).Where(genre => !string.IsNullOrEmpty(genre)).Select(genre => new clsOntologyItem { Name = genre, GUID_Parent = Config.LocalData.Class_Genre.GUID, Type = Globals.Type_Object }).ToList();
                    var composers = mediaItemFiles.SelectMany(genre => genre.Tag.Composers).GroupBy(genre => genre).Select(genre => genre.Key).Where(genre => !string.IsNullOrEmpty(genre)).Select(genre => new clsOntologyItem { Name = genre, GUID_Parent = Config.LocalData.Class_Genre.GUID, Type = Globals.Type_Object }).ToList();

                    request.MessageOutput?.OutputInfo($"Get Partners of Medias...");
                    var getPartnersResult = await serviceAgent.GetMediaPartner();

                    result.ResultState = getPartnersResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    request.MessageOutput?.OutputInfo($"Found {getPartnersResult.Result.Count} Partners of Medias");


                    request.MessageOutput?.OutputInfo($"Check Years...");
                    result.ResultState = await serviceAgent.CheckObjects(years);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    request.MessageOutput?.OutputInfo(result.ResultState.Additional1);


                    request.MessageOutput?.OutputInfo($"Check Albums...");
                    result.ResultState = await serviceAgent.CheckObjects(albums);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    request.MessageOutput?.OutputInfo(result.ResultState.Additional1);

                    request.MessageOutput?.OutputInfo($"Check Genres...");
                    result.ResultState = await serviceAgent.CheckObjects(genres);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                }
                catch (Exception ex)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Error while downloading the Files: {ex.Message}";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }


                request.MessageOutput?.OutputInfo($"Finished tagging");
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<ResultItem<MediaBookmark>>>> SaveBookmarks(SaveBookmarksRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<List<ResultItem<MediaBookmark>>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<ResultItem<MediaBookmark>>()
                };

                var validInvalidBookmarks = ValidationController.GetValidInvalidMediaBookmarks(request.MediaBookMarks, Globals);
                var validBookmarks = validInvalidBookmarks.Where(bookMR => bookMR.ResultState.GUID == Globals.LState_Success.GUID);
                result.Result.AddRange(validInvalidBookmarks.Where(bookMR => bookMR.ResultState.GUID == Globals.LState_Error.GUID));

                var updateBookmarks = request.MediaBookMarks.Where(bookMark => !string.IsNullOrEmpty(bookMark.IdBookmark)).ToList();

                var elasticAgent = new ElasticAgentService(Globals);

                var existingBookmarks = new List<MediaBookmark>();
                if (updateBookmarks.Any())
                {
                    var getMediaBookmarksRequest = new GetMediaBookmarkModelRequest(updateBookmarks.Select(bookMark => bookMark.IdBookmark).ToList(), MediaBookmarkModelRequestType.ByBookmarkIds);
                    var getBookMarksResult = await GetMediaBookmarks(getMediaBookmarksRequest);
                    result.ResultState = getBookMarksResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    existingBookmarks = getBookMarksResult.Result;
                }

                var relationConfig = new clsRelationConfig(Globals);
                var saveObjects = new List<clsOntologyItem>();
                var saveAttributes = new List<clsObjectAtt>();
                var saveRelations = new List<clsObjectRel>();
                var deleteRelations = new List<clsObjectRel>();

                foreach (var mediaBookMark in request.MediaBookMarks)
                {
                    var resultItem = new ResultItem<MediaBookmark>
                    {
                        ResultState = Globals.LState_Success.Clone(),
                        Result = mediaBookMark
                    };

                    result.Result.Add(resultItem);
                    var existingBookMark = existingBookmarks.FirstOrDefault(bookM => bookM.IdBookmark == mediaBookMark.IdBookmark);

                    if (mediaBookMark.IdBookmark == null || existingBookMark == null)
                    {
                        if (mediaBookMark.IdBookmark == null)
                        {
                            mediaBookMark.IdBookmark = Globals.NewGUID;
                        }
                        var bookMarkObject = new clsOntologyItem
                        {
                            GUID = mediaBookMark.IdBookmark,
                            Name = mediaBookMark.NameBookmark,
                            GUID_Parent = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                            Type = Globals.Type_Object
                        };
                        saveObjects.Add(bookMarkObject);

                        saveAttributes.Add(relationConfig.Rel_ObjectAttribute(bookMarkObject, Config.LocalData.AttributeType_Datetimestamp__Create_, mediaBookMark.Created));
                        saveAttributes.Add(relationConfig.Rel_ObjectAttribute(bookMarkObject, Config.LocalData.AttributeType_Second, mediaBookMark.Second));
                        saveRelations.Add(relationConfig.Rel_ObjectRelation(bookMarkObject, new clsOntologyItem
                        {
                            GUID = mediaBookMark.IdMediaItem,
                            Name = mediaBookMark.NameMediaItem,
                            GUID_Parent = Config.LocalData.Class_Media_Item.GUID,
                            Type = Globals.Type_Object
                        }, Config.LocalData.RelationType_belongs_to));
                        saveRelations.Add(relationConfig.Rel_ObjectRelation(bookMarkObject, mediaBookMark.LogState, Config.LocalData.RelationType_is_of_Type));
                        saveRelations.Add(relationConfig.Rel_ObjectRelation(bookMarkObject, mediaBookMark.User, Config.LocalData.RelationType_belongs_to));
                        if (mediaBookMark.References.Any())
                        {
                            saveRelations.AddRange(mediaBookMark.References.Select(refItm => relationConfig.Rel_ObjectRelation(bookMarkObject, refItm, Config.LocalData.RelationType_is_used_by)));
                        }
                    }
                    else
                    {
                        if (mediaBookMark.NameBookmark != existingBookMark.NameBookmark)
                        {
                            saveObjects.Add(new clsOntologyItem
                            {
                                GUID = mediaBookMark.IdBookmark,
                                Name = mediaBookMark.NameBookmark,
                                GUID_Parent = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                                Type = Globals.Type_Object
                            });
                        }

                        if (existingBookMark.IdAttributeCreated == null)
                        {
                            saveAttributes.Add(new clsObjectAtt
                            {
                                ID_Attribute = Globals.NewGUID,
                                ID_AttributeType = Config.LocalData.AttributeType_Datetimestamp__Create_.GUID,
                                ID_DataType = Config.LocalData.AttributeType_Datetimestamp__Create_.GUID_Parent,
                                ID_Object = mediaBookMark.IdBookmark,
                                ID_Class = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                                OrderID = 1,
                                Val_Name = mediaBookMark.Created.ToString(),
                                Val_Date = mediaBookMark.Created
                            });
                        }
                        else if (mediaBookMark.Created != existingBookMark.Created)
                        {
                            saveAttributes.Add(new clsObjectAtt
                            {
                                ID_Attribute = existingBookMark.IdAttributeCreated,
                                ID_AttributeType = Config.LocalData.AttributeType_Datetimestamp__Create_.GUID,
                                ID_DataType = Config.LocalData.AttributeType_Datetimestamp__Create_.GUID_Parent,
                                ID_Object = existingBookMark.IdBookmark,
                                ID_Class = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                                OrderID = 1,
                                Val_Name = mediaBookMark.Created.ToString(),
                                Val_Date = mediaBookMark.Created
                            });
                        }

                        if (existingBookMark.IdAttributeSecond == null)
                        {
                            saveAttributes.Add(new clsObjectAtt
                            {
                                ID_Attribute = Globals.NewGUID,
                                ID_AttributeType = Config.LocalData.AttributeType_Second.GUID,
                                ID_DataType = Config.LocalData.AttributeType_Second.GUID_Parent,
                                ID_Object = mediaBookMark.IdBookmark,
                                ID_Class = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                                OrderID = 1,
                                Val_Name = mediaBookMark.Second.ToString(),
                                Val_Real = mediaBookMark.Second
                            });
                        }
                        else if (mediaBookMark.Second != existingBookMark.Second)
                        {
                            saveAttributes.Add(new clsObjectAtt
                            {
                                ID_Attribute = existingBookMark.IdAttributeSecond,
                                ID_AttributeType = Config.LocalData.AttributeType_Second.GUID,
                                ID_DataType = Config.LocalData.AttributeType_Second.GUID_Parent,
                                ID_Object = existingBookMark.IdBookmark,
                                ID_Class = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                                OrderID = 1,
                                Val_Name = mediaBookMark.Second.ToString(),
                                Val_Real = mediaBookMark.Second
                            });
                        }

                        if (existingBookMark.IdMediaItem == null)
                        {
                            saveRelations.Add(new clsObjectRel
                            {
                                ID_Object = existingBookMark.IdBookmark,
                                ID_Parent_Object = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                                ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_belongs_to_Media_Item.ID_RelationType,
                                ID_Other = mediaBookMark.IdMediaItem,
                                ID_Parent_Other = Config.LocalData.Class_Media_Item.GUID,
                                OrderID = 1,
                                Ontology = Globals.Type_Object
                            });
                        }
                        else if (mediaBookMark.IdMediaItem != existingBookMark.IdMediaItem)
                        {
                            deleteRelations.Add(new clsObjectRel
                            {
                                ID_Object = existingBookMark.IdBookmark,
                                ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_belongs_to_Media_Item.ID_RelationType,
                                ID_Other = existingBookMark.IdMediaItem,
                            });
                            saveRelations.Add(new clsObjectRel
                            {
                                ID_Object = existingBookMark.IdBookmark,
                                ID_Parent_Object = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                                ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_belongs_to_Media_Item.ID_RelationType,
                                ID_Other = mediaBookMark.IdMediaItem,
                                ID_Parent_Other = Config.LocalData.Class_Media_Item.GUID,
                                OrderID = 1,
                                Ontology = Globals.Type_Object
                            });
                        }

                        if (existingBookMark.LogState == null)
                        {
                            saveRelations.Add(new clsObjectRel
                            {
                                ID_Object = mediaBookMark.IdBookmark,
                                ID_Parent_Object = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                                ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_is_of_Type_Logstate.ID_RelationType,
                                ID_Other = mediaBookMark.LogState.GUID,
                                ID_Parent_Other = mediaBookMark.LogState.GUID_Parent,
                                OrderID = 1,
                                Ontology = Globals.Type_Object
                            });
                        }
                        else if (mediaBookMark.LogState.GUID != existingBookMark.LogState.GUID)
                        {
                            deleteRelations.Add(new clsObjectRel
                            {
                                ID_Object = existingBookMark.IdBookmark,
                                ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_is_of_Type_Logstate.ID_RelationType,
                                ID_Other = existingBookMark.LogState.GUID,
                            });
                            saveRelations.Add(new clsObjectRel
                            {
                                ID_Object = existingBookMark.IdBookmark,
                                ID_Parent_Object = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                                ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_is_of_Type_Logstate.ID_RelationType,
                                ID_Other = mediaBookMark.LogState.GUID,
                                ID_Parent_Other = mediaBookMark.LogState.GUID_Parent,
                                OrderID = 1,
                                Ontology = Globals.Type_Object
                            });
                        }

                        if (existingBookMark.User == null)
                        {
                            saveRelations.Add(new clsObjectRel
                            {
                                ID_Object = mediaBookMark.IdBookmark,
                                ID_Parent_Object = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                                ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_belongs_to_user.ID_RelationType,
                                ID_Other = mediaBookMark.User.GUID,
                                ID_Parent_Other = mediaBookMark.User.GUID_Parent,
                                OrderID = 1,
                                Ontology = Globals.Type_Object
                            });
                        }
                        else if (mediaBookMark.User.GUID != existingBookMark.User.GUID)
                        {
                            deleteRelations.Add(new clsObjectRel
                            {
                                ID_Object = existingBookMark.IdBookmark,
                                ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_belongs_to_user.ID_RelationType,
                                ID_Other = existingBookMark.User.GUID,
                            });
                            saveRelations.Add(new clsObjectRel
                            {
                                ID_Object = existingBookMark.IdBookmark,
                                ID_Parent_Object = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                                ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_belongs_to_user.ID_RelationType,
                                ID_Other = mediaBookMark.User.GUID,
                                ID_Parent_Other = mediaBookMark.User.GUID_Parent,
                                OrderID = 1,
                                Ontology = Globals.Type_Object
                            });
                        }

                        deleteRelations.AddRange(from refDb in existingBookMark.References
                                                 join refCheck in mediaBookMark.References on refDb.GUID equals refCheck.GUID into refChecks
                                                 from refCheck in refChecks.DefaultIfEmpty()
                                                 where refCheck == null
                                                 select new clsObjectRel
                                                 {
                                                     ID_Object = existingBookMark.IdBookmark,
                                                     ID_RelationType = Config.LocalData.RelationType_is_used_by.GUID,
                                                     ID_Other = refDb.GUID
                                                 });

                        saveRelations.AddRange(from refCheck in mediaBookMark.References
                                               join refDb in existingBookMark.References on refCheck.GUID equals refDb.GUID into refDbs
                                               from refDb in refDbs.DefaultIfEmpty()
                                               where refDb == null
                                               select relationConfig.Rel_ObjectRelation(new clsOntologyItem
                                               {
                                                   GUID = mediaBookMark.IdBookmark,
                                                   Name = mediaBookMark.NameBookmark,
                                                   GUID_Parent = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                                                   Type = Globals.Type_Object
                                               }, refCheck, Config.LocalData.RelationType_is_used_by));
                    }
                }

                if (saveObjects.Any())
                {
                    var saveResult = await elasticAgent.SaveObjects(saveObjects);
                    result.ResultState = saveResult;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                if (saveAttributes.Any())
                {
                    var saveResult = await elasticAgent.SaveAttributes(saveAttributes);
                    result.ResultState = saveResult;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                if (deleteRelations.Any())
                {
                    var deleteResult = await elasticAgent.DeleteRelations(deleteRelations);
                    result.ResultState = deleteResult;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }
                if (saveRelations.Any())
                {
                    var saveResult = await elasticAgent.SaveRelations(saveRelations);
                    result.ResultState = saveResult;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                return result;
            });
            return taskResult;
        }

        public async Task<ResultItem<List<MediaBookmark>>> GetMediaBookmarks(GetMediaBookmarkModelRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<List<MediaBookmark>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<MediaBookmark>()
                };
                request.MessageOutput?.OutputInfo("Validate request...");
                result.ResultState = ValidationController.ValidateGetMediaBookmarkModelRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Validated request.");

                var elasticAgent = new ElasticAgentService(Globals);

                request.MessageOutput?.OutputInfo("Get model...");
                var mediaBookmarksModelResult = await elasticAgent.GetMediaBookmarkModel(request);
                var mediaBookMarks = mediaBookmarksModelResult.Result;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo($"Have model, Bookmarks: {mediaBookMarks.MediaBookmarks.Count}");

                request.MessageOutput?.OutputInfo($"Apply postfilters: {request.PostFilters.Count}...");
                foreach (var postFilter in request.PostFilters)
                {
                    switch (postFilter.PostFilterRequestType)
                    {
                        case MediaBookmarkModelRequestType.ByBookmarkIds:
                            mediaBookMarks.MediaBookmarks = mediaBookMarks.MediaBookmarks.Where(bookM => postFilter.Ids.Contains(bookM.GUID)).ToList();
                            break;
                        case MediaBookmarkModelRequestType.ByLogstateIds:
                            mediaBookMarks.MediaBookmarksToLogStates = mediaBookMarks.MediaBookmarksToLogStates.Where(logState => postFilter.Ids.Contains(logState.ID_Other)).ToList();
                            break;
                        case MediaBookmarkModelRequestType.ByMediaItemIds:
                            mediaBookMarks.MediaBookmarksToMediaItems = mediaBookMarks.MediaBookmarksToMediaItems.Where(mediaItem => postFilter.Ids.Contains(mediaItem.ID_Other)).ToList();
                            break;
                        case MediaBookmarkModelRequestType.ByReferenceIds:
                            mediaBookMarks.MediaBookmarksToReferences = mediaBookMarks.MediaBookmarksToMediaItems.Where(refItem => postFilter.Ids.Contains(refItem.ID_Other)).ToList();
                            break;
                        case MediaBookmarkModelRequestType.ByUserIds:
                            mediaBookMarks.MediaBookmarksToUsers = mediaBookMarks.MediaBookmarksToUsers.Where(userItem => postFilter.Ids.Contains(userItem.ID_Other)).ToList();
                            break;
                    }
                }
                request.MessageOutput?.OutputInfo($"Applied postfilters.");

                var factory = new MediaBookmarkFactory(Globals);

                request.MessageOutput?.OutputInfo("Create MediaBookmarkList...");
                var factoryResult = await factory.CreateMediaBookmarkList(mediaBookMarks);
                result.ResultState = factoryResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Created MediaBookmarkList.");

                result.Result = factoryResult.Result;

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> DeleteMediaBookmarks(List<MediaBookmark> mediaBookmarks, IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = Globals.LState_Success.Clone();

                var elasticServiceAgent = new ElasticAgentService(Globals);

                var idsToDelete = mediaBookmarks.Select(bookM => bookM.IdBookmark).ToList();

                var deleteResult = await elasticServiceAgent.DeleteObjectsAndAttributesAndRelations(idsToDelete, messageOutput);
                result = deleteResult.ResultState;
                result.Count = deleteResult.Result.ClassAttributes.Count +
                               deleteResult.Result.ClassRelations.Count +
                               deleteResult.Result.Classes.Count +
                               deleteResult.Result.RelationTypes.Count +
                               deleteResult.Result.AttributeTypes.Count +
                               deleteResult.Result.Objects.Count +
                               deleteResult.Result.ObjectAttributes.Count +
                               deleteResult.Result.ObjectRelations.Count;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<RelateBookmarkResult>> RelateBookmarks(RelateBookmarkRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<RelateBookmarkResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new RelateBookmarkResult
                    {
                        MediaBookmarks = request.BookmarkRelationsUpdate.Select(bookM => bookM.MediaBookmark).ToList()
                    }
                };

                request.MessageOutput?.OutputInfo("Validate Request...");
                result.ResultState = ValidationController.ValidateRelateBookmarkRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var elasticServiceAgent = new ElasticAgentService(Globals);
                var relationsToDelete = new List<clsObjectRel>();
                var relationsToCreate = new List<clsObjectRel>();
                var relationConfig = new clsRelationConfig(Globals);
                foreach (var bookmarkRelationUpdate in request.BookmarkRelationsUpdate)
                {
                    var newRefereneces = (from newRef in bookmarkRelationUpdate.NewReferences
                                          join existRef in bookmarkRelationUpdate.MediaBookmark.References on newRef.GUID equals existRef.GUID into existRefs
                                          from existRef in existRefs.DefaultIfEmpty()
                                          where existRef == null
                                          select newRef).ToList();
                     relationsToCreate.AddRange(newRefereneces.Select(refItm => relationConfig.Rel_ObjectRelation(new clsOntologyItem
                     {
                         GUID = bookmarkRelationUpdate.MediaBookmark.IdBookmark,
                         Name = bookmarkRelationUpdate.MediaBookmark.NameBookmark,
                         GUID_Parent = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                         Type = Globals.Type_Object
                     }, refItm, Config.LocalData.RelationType_is_used_by)));
                    relationsToDelete.AddRange(bookmarkRelationUpdate.RemoveReferences.Select(rel => new clsObjectRel
                    {
                        ID_Object = bookmarkRelationUpdate.MediaBookmark.IdBookmark,
                        ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_is_used_by.ID_RelationType,
                        ID_Other = rel.GUID
                    }));
                }

                if (relationsToDelete.Any())
                {
                    result.ResultState = await elasticServiceAgent.DeleteRelations(relationsToDelete);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while deleting the relations of Bookmarks!";
                        return result;
                    }
                }

                if (relationsToCreate.Any())
                {
                    result.ResultState = await elasticServiceAgent.SaveRelations(relationsToCreate);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while creating the new relations of Bookmarks!";
                        return result;
                    }
                }


                foreach (var mediaBookmark in result.Result.MediaBookmarks)
                {
                    mediaBookmark.References = (from refExist in mediaBookmark.References
                                                join delRef in relationsToDelete on new
                                                {
                                                    ID_Object = mediaBookmark.IdBookmark,
                                                    ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_is_used_by.ID_RelationType,
                                                    ID_Other = refExist.GUID
                                                } equals new
                                                {
                                                    delRef.ID_Object,
                                                    delRef.ID_RelationType,
                                                    delRef.ID_Other
                                                } into delRefs
                                                from delRef in delRefs.DefaultIfEmpty()
                                                where delRef == null
                                                select refExist).ToList();
                    var newRefereces = (from newRef in request.BookmarkRelationsUpdate.Where(upd => upd.MediaBookmark.IdBookmark == mediaBookmark.IdBookmark).SelectMany(rel => rel.NewReferences)
                                        join rel in relationsToCreate.Where(rel => rel.ID_Object == mediaBookmark.IdBookmark) on newRef.GUID equals rel.ID_Other
                                        select newRef).ToList();

                    mediaBookmark.References.AddRange(newRefereces.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.GUID,
                        Name = rel.Name,
                        GUID_Parent = rel.GUID_Parent,
                        Type = rel.Type
                    }));
                }
                
                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> RemoveRelation(RemoveReferencesRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = Globals.LState_Success.Clone();

                request.MessageOutput?.OutputInfo("Validate Request...");
                result = ValidationController.ValidateRemoveBookmarkRequest(request, Globals);

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }


                request.MessageOutput?.OutputInfo("Validated Request.");

                var elasticServiceAgent = new ElasticAgentService(Globals);
                var relationsToDelete = request.MediaBookmarkReferences.Select(refItm => new clsObjectRel
                {
                    ID_Object = refItm.IdMediaBookmark,
                    ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_is_used_by.ID_RelationType,
                    ID_Other = refItm.IdReference
                }).ToList();

                var dbDeletor = new OntologyModDBConnector(Globals);

                if (relationsToDelete.Any())
                {
                    request.MessageOutput?.OutputInfo($"Delete {relationsToDelete.Count} relations...");
                    result = dbDeletor.DelObjectRels(relationsToDelete);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while deleting the relations!";
                        return result;
                    }
                    request.MessageOutput?.OutputInfo($"Deleted relations.");
                }

                return result;
            });

            return taskResult;
        }

        public MediaTaggingController(Globals globals) : base(globals)
        {
            mediaStoreConnector = new MediaStoreConnector(Globals);
        }
    }
}
