﻿using MediaViewerModule.Models;
using MediaViewerModule.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Converters
{
    public static class DictionaryToMediaListItem
    {
        public static MediaListItem Convert(Dictionary<string, object> dictItem, MultimediaItemType itemType)
        {
            var applyStr = ((string[])dictItem[nameof(MediaListItem.Apply)])[0];
            var apply = false;

            if (!string.IsNullOrEmpty(applyStr))
            {
                apply = bool.Parse(applyStr);
            }

            DateTime? fileCreate = null;

            var orderIdStr = ((string[])dictItem[nameof(MediaListItem.OrderId)])[0];
            long orderId = 0;
            if (!string.IsNullOrEmpty(orderIdStr))
            {
                orderId = long.Parse(orderIdStr);
            }

            var randomStr = ((string[])dictItem[nameof(MediaListItem.Random)])[0];
            long random = 0;
            if (!string.IsNullOrEmpty(randomStr))
            {
                random = long.Parse(randomStr);
            }

            var bookMarkStr = ((string[])dictItem[nameof(MediaListItem.BookmarkCount)])[0];
            long bookMarkCount = 0;
            if (!string.IsNullOrEmpty(bookMarkStr))
            {
                bookMarkCount = long.Parse(bookMarkStr);
            }

            var result = new MediaListItem
            {
                IdRef = ((string[])dictItem[nameof(MediaListItem.IdRef)])[0],
                IdMultimediaItem = ((string[])dictItem[nameof(MediaListItem.IdMultimediaItem)])[0],
                IdAttributeCreateDate = ((string[])dictItem[nameof(MediaListItem.IdAttributeCreateDate)])[0],
                IdClassMultimediaItem = ((string[])dictItem[nameof(MediaListItem.IdClassMultimediaItem)])[0],
                IdFile = ((string[])dictItem[nameof(MediaListItem.IdFile)])[0],
                Apply = apply,
                BookmarkCount = bookMarkCount,
                FileCreateDate = fileCreate,
                FileUrl = ((string[])dictItem[nameof(MediaListItem.FileUrl)])[0],
                MediaType = itemType,
                MimeType = ((string[])dictItem[nameof(MediaListItem.MimeType)])[0],
                NameFile = ((string[])dictItem[nameof(MediaListItem.NameFile)])[0],
                NameMultimediaItem = ((string[])dictItem[nameof(MediaListItem.NameMultimediaItem)])[0],
                NameRef = ((string[])dictItem[nameof(MediaListItem.NameRef)])[0],
                OrderId = orderId,
                PosterUrl = ((string[])dictItem[nameof(MediaListItem.PosterUrl)])[0],
                Random = random,
                ThumbFileUrl = ((string[])dictItem[nameof(MediaListItem.ThumbFileUrl)])[0],

            };

            return result;
        }
    }
}
