﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class PDFDocumentsRawResult
    {
        public List<clsOntologyItem> PDFDocuments { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> RefItems { get; set; } = new List<clsObjectRel>();

    }
}
