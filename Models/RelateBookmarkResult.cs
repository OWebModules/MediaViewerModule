﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class RelateBookmarkResult
    {
        public List<MediaBookmark> MediaBookmarks { get; set; } = new List<MediaBookmark>();
    }
}
