﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class BookmarkRelationsUpdate
    {
        public MediaBookmark MediaBookmark { get; set; }
        public List<clsOntologyItem> NewReferences { get; set; } = new List<clsOntologyItem>();
        public List<clsOntologyItem> RemoveReferences { get; set; } = new List<clsOntologyItem>();
    }
}
