﻿using ElasticSearchConfigModule.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class OCRImagesModel
    {
        public clsOntologyItem Config { get; set; }
        public List<clsObjectRel> SessionOCRs { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> SessionOCRsNotExisting { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> SessionOCRsError { get; set; } = new List<clsObjectRel>();

        public List<MediaListItem> Images { get; set; } = new List<MediaListItem>();
        public ElasticSearchConfig ElasticSearchConfig { get; set; }
        public clsObjectRel ConfigToPath { get; set; }
    }
}
