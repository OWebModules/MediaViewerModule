﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class GetImageMapModel
    {
        public clsOntologyItem Reference { get; set; }
        public clsOntologyItem ImageMap { get; set; }
        public List<clsObjectRel> ImageMapsToImageAreas { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> ImageAreasToCoords { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> ImageAreasToShapes { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ImageAreasToObjects { get; set; } = new List<clsObjectRel>();
    }
}
