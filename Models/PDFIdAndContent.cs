﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class PDFIdAndContent
    {
        public string Id { get; set; }
        public string Content { get; set; }
        public long Page { get; set; }
    }
}
