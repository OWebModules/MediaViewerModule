﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TagLib;

namespace MediaViewerModule.Models
{
    public class TagMediaItemsResult
    {
        public List<TaggedMediaItem> Tagged { get; set; } = new List<TaggedMediaItem>();
    }

    public class TaggedMediaItem
    {
        public Tag Tag { get; set; }
        public clsOntologyItem MediaItem { get; set; }
        public clsOntologyItem FileItem { get; set; }
    }
}
