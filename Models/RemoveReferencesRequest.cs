﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class RemoveReferencesRequest
    {
        public List<MediaBookmarkReference> MediaBookmarkReferences { get; private set; } = new List<MediaBookmarkReference>();

        public IMessageOutput MessageOutput { get; set; }

        public RemoveReferencesRequest(List<MediaBookmarkReference> mediaBookmarkReferences)
        {
            MediaBookmarkReferences = mediaBookmarkReferences;
        }

    }

    public class MediaBookmarkReference
    {
        public string IdMediaBookmark { get; set; }
        public string IdReference {get; set; }
    }
}
