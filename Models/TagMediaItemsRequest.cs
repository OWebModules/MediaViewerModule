﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public enum MediaItemType
    {
        Image = 0,
        Audio = 1,
        Video = 2
    }
    public class TagMediaItemsRequest
    {
        public MediaItemType MediaItemType { get; private set; }
        public List<clsOntologyItem> ItemFilter { get; set; } = new List<clsOntologyItem>();
        public List<clsOntologyItem> Related { get; set; } = new List<clsOntologyItem>();
        public string NameRegex { get; set; }
        public string MediaStorePath { get; private set; }

        public IMessageOutput MessageOutput { get; set; }

        public TagMediaItemsRequest(MediaItemType mediaItemType, string mediaStorePath)
        {
            MediaItemType = mediaItemType;
            MediaStorePath = mediaStorePath;
        }
    }
}
