﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    [KendoGridConfig(
       groupbable = true,
       autoBind = false,
       scrollable = true,
       resizable = true,
       selectable = SelectableType.multipleCell,
       editable = EditableType.False,
       height = "100%",
        toolbar = ToolbarType.Search)]
    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    public class OCRSearchGridItem
    {

        [KendoColumn(hidden = true)]
        public string IdOCRDocument { get; set; }

        [KendoColumn(hidden = false, Order = 0, filterable = false, title = "Document")]
        [KendoColumnSortable(sortable = false)]
        public string NameOCRDocument { get; set; }

        [KendoColumn(hidden = true)]
        public string IdReferenceClass { get; set; }

        [KendoColumn(hidden = false, Order = 1, filterable = false, title = "Class")]
        [KendoColumnSortable(sortable = false)]
        public string NameReferenceClass { get; set; }

        [KendoColumn(hidden = true)]
        public string IdReference { get; set; }

        [KendoColumn(hidden = false, Order = 2, filterable = false, title = "Reference")]
        [KendoColumnSortable(sortable = false)]
        public string NameReference { get; set; }

        [KendoColumn(hidden = false, Order = 3, filterable = false, title = "Content")]
        [KendoColumnSortable(sortable = false)]
        public string Content { get; set; }
    }
}
