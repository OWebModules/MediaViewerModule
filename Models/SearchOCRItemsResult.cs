﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class SearchOCRItemsResult
    {
        public string ScrollId { get; set; }
        public long TotalCount { get; set; }
        public List<OCRSearchGridItem> GridItems { get; set; } = new List<OCRSearchGridItem>();
    }
}
