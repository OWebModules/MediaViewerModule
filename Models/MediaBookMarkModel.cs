﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class MediaBookMarkModel
    {
        public List<clsOntologyItem> MediaBookmarks { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> MediaBookmarksToLogStates { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> MediaBookmarksToMediaItems { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> MediaBookmarksToUsers { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> MediaBookmarksToReferences { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> MediaBookmarkCreationDates { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> MediaBookmarkSecond { get; set; } = new List<clsObjectAtt>();

    }
}
 