﻿using OntologyAppDBConnector.Base;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.row,
        editable = EditableType.incell,
        height = "100%")]
    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoNumberFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal")]
    [KendoBoolFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", istrue = "W", isfalse = "F")]
    [KendoDateFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class MediaListItem : MultiMediaItem
    {
        //[KendoColumn(hidden = false, Order = 1, filterable = false, title = "Apply", template = "<input type=\"checkbox\" #= Apply ? 'checked=\"checked\"' : \"\" # 'class=\"chkbxApply\"' />", width = "80px")]
        [KendoColumn(hidden = false, Order = 0, filterable = false, title = "Apply", template = "<input type=\"checkbox\" #= Apply ? 'checked=\"checked\"' : \"\" # class=\"chkbxApply\" />", width = "80px", type = ColType.BooleanType)]
        [KendoModel(editable = true)]
        public bool Apply { get; set; }

        [KendoColumn(hidden = false, Order = 2, filterable = false, title = "Order", type = ColType.NumberType)]
        [KendoModel(editable = true)]
        public long? OrderId { get; set; }

        [KendoColumn(hidden = false, Order = 3, filterable = false, title = "Bookmark-Count", type = ColType.NumberType)]
        public long? BookmarkCount { get; set; }

        [KendoColumn(hidden = false, Order = 4, filterable = false, title = "Random", type = ColType.NumberType)]
        public long? Random { get; set; }

        [KendoColumn(hidden = true)]
        public string FileUrl { get; set; }

        [KendoColumn(hidden = true)]
        public string ThumbFileUrl { get; set; }

        [KendoColumn(hidden = true)]
        public string PosterUrl { get; set; } = "/Resources/Images/Ontology-Module2.png";

        [KendoColumn(hidden = true)]
        public string title
        {
            get
            {
                return NameMultimediaItem;
            }
        }

        [KendoColumn(hidden = true)]
        public string poster { get { return PosterUrl; } }

        [KendoColumn(hidden = true)]
        public string source { get { return FileUrl; } }

    }
}
