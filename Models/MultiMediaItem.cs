﻿using MediaViewerModule.Primitives;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    
    public class MultiMediaItem
    {
        [KendoColumn(hidden = true)]
        public string IdMultimediaItem { get; set; }
        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Name")]
        [KendoModel(editable = true)]
        public string NameMultimediaItem { get; set; }
        [KendoColumn(hidden = true)]
        public string IdClassMultimediaItem { get; set; }
        [KendoColumn(hidden = true)]
        public string IdFile { get; set; }
        [KendoColumn(hidden = true)]
        public string NameFile { get; set; }
        [KendoColumn(hidden = true)]
        public string IdAttributeCreateDate { get; set; }
        [KendoColumn(hidden = false, Order = 5, filterable = true, title = "Date", template = "#= FileCreateDate != null ? kendo.toString(kendo.parseDate(FileCreateDate), 'dd.MM.yyyy HH:mm:ss') : '' #", type = ColType.DateType)]
        public DateTime? FileCreateDate { get; set; }
        [KendoColumn(hidden = true)]
        public string IdRef { get; set; }
        [KendoColumn(hidden = true)]
        public string NameRef { get; set; }
        [KendoColumn(hidden = true)]
        public MultimediaItemType MediaType { get; set; }
        [KendoColumn(hidden = true)]
        public string MimeType { get; set; }
    }
}
