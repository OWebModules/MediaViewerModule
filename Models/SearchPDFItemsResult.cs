﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class SearchPDFItemsResult
    {
        public string ScrollId { get; set; }
        public long TotalCount { get; set; }
        public List<PDFSearchGridItem> GridItems { get; set; } = new List<PDFSearchGridItem>();
    }
}
