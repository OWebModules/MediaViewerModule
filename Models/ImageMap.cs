﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class ImageMap
    {
        public string IdReference { get; set; }
        public string NameReference { get; set; }
        public string IdImageMap { get; set; }
        public string NameImageMap { get; set; }

        public string ImageUrl { get; set; }

        public List<ImageArea> ImageAreas { get; set; } = new List<ImageArea>();

    }

    public class ImageArea
    {
        public string IdImageArea { get; set; }
        public string NameImageArea { get; set; }

        public string IdAttributeCoords { get; set; }

        public string Coords { get; set; }

        public string IdShape { get; set; }

        public string NameShape { get; set; }

        public string IdObject { get; set; }
        public string NameObject { get; set; }

    }
}
