﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class OCRIdAndContent
    {
        public string Id { get; set; }
        public string Content { get; set; }
    }
}
