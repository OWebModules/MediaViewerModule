﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public static class OCRIndex
    {
        public static string FieldId => "ImageId";
        public static string FieldPage => "Page";
        public static string FieldContent => "Content";

    }
}
