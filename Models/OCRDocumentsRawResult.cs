﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class OCRDocumentsRawResult
    {
        public List<clsOntologyItem> OCRDocuments { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> RefItems { get; set; } = new List<clsObjectRel>();

    }
}
