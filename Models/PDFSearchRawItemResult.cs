﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class PDFSearchRawItemResult
    {
        public string SearchString { get; set; }
        public List<clsOntologyItem> PDFDocuments { get; set; }
        public List<clsObjectRel> PDFDocumentsToReferences { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> PDFDocumentsToFiles { get; set; } = new List<clsObjectRel>();
    }
}
