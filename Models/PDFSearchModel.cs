﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class PDFSearchModel
    {
        public clsOntologyItem BaseConfig { get; set; }
        public clsObjectRel BaseConfigToEsIndex { get; set; }
        public clsObjectRel BaseConfigToEsType { get; set; }
    }
}
