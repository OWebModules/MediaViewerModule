﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class AssociateMediaItemsResult
    {
        public List<clsOntologyItem> AssociatedMediaItems { get; set; } = new List<clsOntologyItem>();
        public List<clsOntologyItem> AssociatedFiles { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> AssociatedRelations { get; set; } = new List<clsObjectRel>();
    }
}
