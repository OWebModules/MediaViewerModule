﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class RelateBookmarkRequest
    {
        public List<BookmarkRelationsUpdate> BookmarkRelationsUpdate { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public RelateBookmarkRequest(List<BookmarkRelationsUpdate> bookmarkRelationsUpdate)
        {
            BookmarkRelationsUpdate = bookmarkRelationsUpdate;
        }
    }

}
