﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public enum MediaBookmarkModelRequestType
    {
        ByBookmarkIds,
        ByMediaItemIds,
        ByReferenceIds,
        ByUserIds,
        ByLogstateIds
    }
    public class GetMediaBookmarkModelRequest
    {
        public MediaBookmarkModelRequestType RequestType { get; private set; }
        public List<string> Ids { get; private set; }
        public List<MediaBookMarkPostFilter> PostFilters { get; set; } = new List<MediaBookMarkPostFilter>();

        public IMessageOutput MessageOutput { get; set; }

        public GetMediaBookmarkModelRequest(List<string> ids, MediaBookmarkModelRequestType requestType)
        {
            RequestType = requestType;
            Ids = ids;
        }

        

    }

    public class MediaBookMarkPostFilter
    {
        public MediaBookmarkModelRequestType PostFilterRequestType { get; private set; }
        public List<string> Ids { get; private set; }

        public MediaBookMarkPostFilter(List<string> ids, MediaBookmarkModelRequestType postFilterRequestType)
        {
            PostFilterRequestType = postFilterRequestType;
            Ids = ids;
        }

    }
}
