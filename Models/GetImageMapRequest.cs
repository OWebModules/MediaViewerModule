﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class GetImageMapRequest
    {
        public string IdReference { get; private set; }
        public string MediaStorePath { get; private set; }
        public string MediaStoreUrl { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public GetImageMapRequest(string idReference, string mediaStorePath, string mediaStoreUrl)
        {
            IdReference = idReference;
            MediaStorePath = mediaStorePath;
            MediaStoreUrl = mediaStoreUrl;
        }
    }
}
