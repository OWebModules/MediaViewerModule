﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class SaveBookmarksRequest
    {
        public List<MediaBookmark> MediaBookMarks { get; private set; } = new List<MediaBookmark>();
        public IMessageOutput MessageOutput { get; set; }

        public SaveBookmarksRequest(List<MediaBookmark> mediaBookMarks)
        {
            MediaBookMarks = mediaBookMarks;
        }
    }
}
