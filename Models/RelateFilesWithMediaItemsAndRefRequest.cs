﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class RelateFilesWithMediaItemsAndRefRequest
    {
        public clsOntologyItem RefItem { get; set; }
        public List<clsOntologyItem> FileItems { get; set; } = new List<clsOntologyItem>();
        public MediaViewerModule.Primitives.MultimediaItemType MediaType { get; set; }
    }
}
