﻿using MediaViewerModule.Services;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    
    public class AssociateMediaItemsRequest
    {
        
        public List<AssociateMediaItemsRequestFilter> Filters { get; private set; } = new List<AssociateMediaItemsRequestFilter>();
        public clsOntologyItem RefItem { get; set; }
        public string ValidationMessage { get; private set; }
        public AssociationType TypeOfAssociation { get; private set; }

        private bool? isValid;
        public bool IsValid
        {
            get
            {
                if (isValid == null)
                {
                    if (!Filters.Any())
                    {
                        isValid = false;
                    }
                    var notValidItems = !Filters.Any(filter => filter.FileItem == null || filter.FileItem.GUID_Parent != MediaViewerModule.Connectors.MediaViewerConnector.ClassFile.GUID);

                    if (!notValidItems)
                    {
                        ValidationMessage = "There are invalid Items!";
                        isValid = false;
                    }

                    if (Filters.Any(filter => filter.RegexName))
                    {
                        try
                        {
                            var regex = new Regex($"({ string.Join("|", Filters.Where(filter => filter.RegexName && !string.IsNullOrEmpty(filter.FileItem.Name)).Select(filter => filter.FileItem.Name))})");
                            regexFilter = regex;
                        }
                        catch (Exception ex)
                        {
                            ValidationMessage = $"Error Regex: {ex.Message}";
                            isValid = false;
                        }
                    }
                    
                    isValid = true;
                }

                return isValid.Value;
            }
            
        }

        private Regex regexFilter = null;
        public Regex RegexFilter
        {
            get
            {
                return regexFilter;
            }
        }


        public AssociateMediaItemsRequest(List<AssociateMediaItemsRequestFilter> filters, AssociationType typeOfAssiciation)
        {
            Filters = filters;
            var result = IsValid;
            TypeOfAssociation = typeOfAssiciation;
        }

    }

    public class AssociateMediaItemsRequestFilter
    {
        public clsOntologyItem FileItem { get; set; }
        public bool RegexName { get; set; }
    }
}
