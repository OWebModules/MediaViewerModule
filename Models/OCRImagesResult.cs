﻿using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class OCRImagesResult
    {
        public clsObjectAtt SessionStamp { get; set; }
        public List<ResultItem<clsOntologyItem>> OCRResult { get; set; } = new List<ResultItem<clsOntologyItem>>();
    }
}
