﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    [KendoGridConfig(
       groupbable = true,
       autoBind = false,
       scrollable = true,
       resizable = true,
       selectable = SelectableType.multipleCell,
       editable = EditableType.False,
       height = "100%",
        toolbar = ToolbarType.Search)]
    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    public class PDFSearchGridItem
    {

        [KendoColumn(hidden = true)]
        public string IdPDFDocument { get; set; }

        [KendoColumn(hidden = false, Order = 0, filterable = false, title = "Document", template = "<a href='#= PDFViewerUrl #' target='_blank'>#= NamePFDocument #</a>")]
        [KendoColumnSortable(sortable = false)]
        public string NamePFDocument { get; set; }

        [KendoColumn(hidden = true)]
        public string IdReferenceClass { get; set; }

        [KendoColumn(hidden = false, Order = 1, filterable = false, title = "Class")]
        [KendoColumnSortable(sortable = false)]
        public string NameReferenceClass { get; set; }

        [KendoColumn(hidden = true)]
        public string IdReference { get; set; }

        [KendoColumn(hidden = false, Order = 2, filterable = false, title = "Reference")]
        [KendoColumnSortable(sortable = false)]
        public string NameReference { get; set; }

        [DataViewColumn(IsVisible = true, DisplayOrder = 2, CellType = CellType.Integer)]
        [KendoColumn(hidden = false,
            Order = 3,
            filterable = false,
            title = "Action",
            width = "120px",
            template = "<button type='button' class='pdf-search-ref-edit'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button><button type='button' class='button-module-starter-pdf-reference'><i class='fa fa-external-link-square' aria-hidden='true'></i></button>")]
        [KendoModel(editable = false)]
        public int Actions { get; set; }

        [KendoColumn(hidden = false, Order = 4, filterable = false, title = "Content")]
        [KendoColumnSortable(sortable = false)]
        public string Content { get; set; }

        [KendoColumn(hidden = false, Order = 5, filterable = false, title = "Page")]
        [KendoColumnSortable(sortable = false)]
        public long Page { get; set; }

        [KendoColumn(hidden = true)]
        public string PDFViewerUrl { get; set; }
    }
}
