﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class RelateFilesWithMediaItemsAndRefResult : RelateFilesWithMediaItemsAndRefRequest
    {
        public List<MediaListItem> MediaItems { get; set; } = new List<MediaListItem>();
    }
}
