﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class ResultMediaListItem
    {
        public clsOntologyItem Result { get; set; }
        public List<MediaListItem> MediaListItems { get; set;}
    }
}
