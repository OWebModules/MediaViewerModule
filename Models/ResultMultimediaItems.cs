﻿using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class ResultMultimediaItems
    {
        public clsOntologyItem Result { get; set; }
        public List<clsOntologyItem> RefItems { get; set; } = new List<clsOntologyItem>();
        public List<clsOntologyItem> MediaItems { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> MediaItemsToRef { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> FilesToCreateDate { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> MediaItemsToFiles { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> BookmarksToMediaItems { get; set; } = new List<clsObjectRel>();
    }
}
