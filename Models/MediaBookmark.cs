﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public enum ChangeState
    {
        None,
        New,
        Changed,
        Delete
    }
    public class MediaBookmark
    {
        public string IdBookmark { get; set; }
        public string NameBookmark { get; set; }

        public string IdAttributeSecond { get; set;}
        public double Second { get; set; }

        public string IdAttributeCreated { get; set; }
        public DateTime Created { get; set; }
        public string IdMediaItem { get; set; }
        public string NameMediaItem { get; set; }
        public clsOntologyItem LogState { get; set; }

        public clsOntologyItem User { get; set; }

        public List<clsOntologyItem> References { get; set; } = new List<clsOntologyItem>();
    }
}
