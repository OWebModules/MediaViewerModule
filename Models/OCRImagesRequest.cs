﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class OCRImagesRequest
    {
        public string IdConfig { get; private set; }
        public CancellationToken CancellationToken { get; set; }
        public IMessageOutput MessageOutput { get; set; }

        public OCRImagesRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
