﻿using OntologyAppDBConnector;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Models
{
    public class SearchPDFItemsRequest
    {
        public string SearchString { get; private set; }
        public int PageSize { get; set; } = 20;
        public int Page { get; set; } = 0;
        public string ScrollId { get; set; }

        public string PDFViewerAction { get; set; }
        public List<KendoSortRequest> SortRequests { get; set; } = new List<KendoSortRequest>();
        public IMessageOutput MessageOutput { get; set; }

        public SearchPDFItemsRequest(string searchString)
        {
            SearchString = searchString;
        }
    }
}
