﻿using MediaViewerModule.Models;
using MediaViewerModule.Primitives;
using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Factories
{
    public class MediaItemListFactory
    {
        private Globals globals;

        public async Task<ResultMediaListItem> GetMediaItemList(ResultMultimediaItems resultMediaListItems)
        {
            var taskResult = await Task.Run<ResultMediaListItem>(() =>
            {
                var result = new ResultMediaListItem
                {
                    Result = globals.LState_Success.Clone()
                };

                var rnd = new Random();

                var mediaListItems = (from mediaItemToRef in resultMediaListItems.MediaItemsToRef
                                      join mediaItemToFile in resultMediaListItems.MediaItemsToFiles on mediaItemToRef.ID_Object equals mediaItemToFile.ID_Object
                                      join fileToCreateDate in resultMediaListItems.FilesToCreateDate on mediaItemToFile.ID_Other equals fileToCreateDate.ID_Object into filesToCreateDates
                                      from fileToCreateDate in filesToCreateDates.DefaultIfEmpty()
                                      select new MediaListItem
                                      {
                                          IdMultimediaItem = mediaItemToRef.ID_Object,
                                          NameMultimediaItem = mediaItemToRef.Name_Object,
                                          IdFile = mediaItemToFile.ID_Other,
                                          NameFile = mediaItemToFile.Name_Other,
                                          IdRef = mediaItemToRef.ID_Other,
                                          NameRef = mediaItemToRef.Name_Other,
                                          OrderId = mediaItemToRef.OrderID,
                                          IdAttributeCreateDate = fileToCreateDate != null ? fileToCreateDate.ID_Attribute : null,
                                          FileCreateDate = fileToCreateDate != null ? fileToCreateDate.Val_Date : null,
                                          Random = rnd.Next(1, Int32.MaxValue),
                                          IdClassMultimediaItem = mediaItemToRef.ID_Parent_Object,
                                          MediaType = MediaTypes.GetMediaTypeOfFile(mediaItemToFile.Name_Other),
                                          MimeType = MediaTypes.GetMimeTypeOfFile(mediaItemToFile.Name_Other)
                                      }).OrderBy(medItem => medItem.OrderId).ThenBy(medItem => medItem.NameMultimediaItem).ToList();

                if (resultMediaListItems.BookmarksToMediaItems != null)
                {
                    var bookMarkCounts = resultMediaListItems.BookmarksToMediaItems.GroupBy(bookMark => new { ID_MediaItem = bookMark.ID_Other, ID_BookMark = bookMark.ID_Object });

                    mediaListItems.ForEach(mediaListItem =>
                    {
                        mediaListItem.BookmarkCount = bookMarkCounts.Where(bookMarkCnt => bookMarkCnt.Key.ID_MediaItem == mediaListItem.IdMultimediaItem).Count();
                    });
                }
                
                result.MediaListItems = mediaListItems;
                return result;
            });

            return taskResult;
        }
        
        public MediaItemListFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
