﻿using MediaViewerModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Factories
{
    public static class OCRSearchGridViewItemFactory
    {
        public static async Task<ResultItem<List<OCRSearchGridItem>>> CreateGridViewItemList(OCRDocumentsRawResult rawList, List<OCRIdAndContent> pdfIdsAndDocs, Globals globals, string searchString)
        {
            var taskResult = await Task.Run<ResultItem<List<OCRSearchGridItem>>>(() =>
           {
               var result = new ResultItem<List<OCRSearchGridItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<OCRSearchGridItem>()
               };

               
               result.Result = (from pdf in rawList.OCRDocuments
                                join refItem in rawList.RefItems on pdf.GUID equals refItem.ID_Object into refItems
                                join doc in pdfIdsAndDocs on pdf.GUID equals doc.Id
                                from refItem in refItems.DefaultIfEmpty()
                                select new OCRSearchGridItem
                                {
                                    IdOCRDocument = pdf.GUID,
                                    NameOCRDocument = pdf.Name,
                                    IdReference = refItem?.ID_Other,
                                    NameReference = refItem?.Name_Other,
                                    IdReferenceClass = refItem.ID_Parent_Other,
                                    NameReferenceClass = refItem.Name_Parent_Other,
                                    Content = doc.Content
                                }).OrderBy(res => res.NameReference).ToList();

               //var offset = 0;

               //foreach (var resultItem in result.Result)
               //{
               //    var lastIndexOfContent = resultItem.Content.Length - 1;
               //    var indexStart = resultItem.Content.ToLower().IndexOf(searchString.ToLower());
               //    var indexEnd = searchString.Length;
               //    var firstIndex = indexStart >= offset ? indexStart - offset : 0;
               //    var lastIndex = indexEnd + offset <= lastIndexOfContent ? indexEnd + offset : lastIndexOfContent;
               //    resultItem.Content = resultItem.Content.Substring(firstIndex, lastIndex);
               //}

               return result;
           });

            return taskResult;
        }
    }
}
