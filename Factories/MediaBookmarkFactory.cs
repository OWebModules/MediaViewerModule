﻿using MediaViewerModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Factories
{
    public class MediaBookmarkFactory
    {
        private Globals globals;

        public async Task<ResultItem<List<MediaBookmark>>> CreateMediaBookmarkList(MediaBookMarkModel model)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<List<MediaBookmark>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var references = model.MediaBookmarksToReferences.Select(refItm => new clsOntologyItem
                {
                    GUID = refItm.ID_Other,
                    Name = refItm.Name_Other,
                    GUID_Parent = refItm.ID_Parent_Other,
                    Type = refItm.Ontology,
                    GUID_Related = refItm.ID_Object
                }).ToList();
                result.Result = (from bookMark in model.MediaBookmarks
                                 join createStamp in model.MediaBookmarkCreationDates on bookMark.GUID equals createStamp.ID_Object
                                 join second in model.MediaBookmarkSecond on bookMark.GUID equals second.ID_Object
                                 join logstate in model.MediaBookmarksToLogStates on bookMark.GUID equals logstate.ID_Object
                                 join mediaItem in model.MediaBookmarksToMediaItems on bookMark.GUID equals mediaItem.ID_Object
                                 join user in model.MediaBookmarksToUsers on bookMark.GUID equals user.ID_Object
                                 select new MediaBookmark
                                 {
                                     IdBookmark = bookMark.GUID,
                                     NameBookmark = bookMark.Name,
                                     IdMediaItem = mediaItem.ID_Other,
                                     NameMediaItem = mediaItem.Name_Other,
                                     LogState = new clsOntologyItem
                                     {
                                         GUID = logstate.ID_Other,
                                         Name = logstate.Name_Other,
                                         GUID_Parent = logstate.ID_Parent_Other,
                                         Type = logstate.Ontology
                                     },
                                     User = new clsOntologyItem
                                     {
                                         GUID = user.ID_Other,
                                         Name = user.Name_Other,
                                         GUID_Parent = user.ID_Parent_Other,
                                         Type = user.Ontology
                                     },
                                     IdAttributeCreated = createStamp.ID_Attribute,
                                     Created = createStamp.Val_Date.Value,
                                     IdAttributeSecond = second.ID_Attribute,
                                     Second = second.Val_Double.Value,
                                     References = references.Where(refItm => refItm.GUID_Related == bookMark.GUID).ToList()
                                 }).ToList();


                return result;
            });
            return taskResult;
        }

        public MediaBookmarkFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
