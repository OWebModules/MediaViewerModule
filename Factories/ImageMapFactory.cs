﻿using MediaViewerModule.Models;
using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Factories
{
    public class ImageMapFactory
    {
        public Globals Globals { get; private set; }

        public async Task<ResultItem<Models.ImageMap>> CreateImageMap(GetImageMapModel model)
        {
            var taskResult = await Task.Run<ResultItem<Models.ImageMap>>(() =>
           {
               var result = new ResultItem<Models.ImageMap>
               {
                   ResultState = Globals.LState_Success.Clone()
               };

               if (model.ImageMap == null)
               {
                   return result;
               }
               result.Result = new Models.ImageMap();

               result.Result.IdReference = model.Reference.GUID;
               result.Result.NameReference = model.Reference.Name;
               result.Result.IdImageMap = model.ImageMap.GUID;
               result.Result.NameImageMap = model.ImageMap.Name;
               result.Result.ImageAreas = (from area in model.ImageMapsToImageAreas
                                           join coord in model.ImageAreasToCoords on area.ID_Other equals coord.ID_Object
                                           join shape in model.ImageAreasToShapes on area.ID_Other equals shape.ID_Object
                                           join obj in model.ImageAreasToObjects on area.ID_Other equals obj.ID_Object
                                           select new ImageArea
                                           {
                                               IdAttributeCoords = coord.ID_Attribute,
                                               Coords = coord.Val_String,
                                               IdImageArea = area.ID_Other,
                                               NameImageArea = area.Name_Other,
                                               IdObject = obj.ID_Other,
                                               NameObject = obj.Name_Other,
                                               IdShape = shape.ID_Other,
                                               NameShape = shape.Name_Other
                                           }).ToList();
               return result;
           });
            return taskResult;
        }

        public ImageMapFactory(Globals globals)
        {
            Globals = globals;
        }
    }
}
