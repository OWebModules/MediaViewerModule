﻿using ElasticSearchConfigModule;
using MediaViewerModule.Models;
using MediaViewerModule.Primitives;
using MediaViewerModule.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyAppDBConnector.Models;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Services
{

    public enum AssociationType
    {
        Image = 0,
        MediaItem = 1,
        PDF = 2
    }

    public class ElasticAgentService : ElasticBaseAgent
    {
        private const string ERROR_LOGSTATES = "Error while getting the relation between the Bookmarks and the Logstates!";
        private const string ERROR_MEDIAITEMS = "Error while getting the relation between the Bookmarks and the MediaItems!";
        private const string ERROR_REFERENCES = "Error while getting the relation between the Bookmarks and the References!";
        private const string ERROR_USERS = "Error while getting the relation between the Bookmarks and the Users!";
        private clsRelationConfig relationConfig;


        public async Task<ResultMultimediaItems> SearchMediaItems(List<clsOntologyItem> refItems, clsOntologyItem mediaItemType)
        {

            var taskResult = await Task.Run<ResultMultimediaItems>(() =>
            {
                var result = new ResultMultimediaItems
                {
                    RefItems = refItems,
                    Result = globals.LState_Success.Clone()
                };

                clsOntologyItem resultDbReader = globals.LState_Success.Clone();

                var dbReaderObjects = new OntologyModDBConnector(globals);

                if (refItems.Any())
                {
                    resultDbReader = dbReaderObjects.GetDataObjects(refItems);

                    if (resultDbReader.GUID == globals.LState_Error.GUID)
                    {
                        result.Result = globals.LState_Error.Clone();
                        return result;

                    }

                    refItems = dbReaderObjects.Objects1;
                }
                else
                {
                    var searchRefItems = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            GUID_Parent = mediaItemType.GUID
                        }
                    };

                    resultDbReader = dbReaderObjects.GetDataObjects(searchRefItems);
                    if (resultDbReader.GUID == globals.LState_Error.GUID)
                    {
                        result.Result = globals.LState_Error.Clone();
                        return result;

                    }

                    refItems = dbReaderObjects.Objects1;

                }
                var refItemsNoMediaItems = refItems.Where(refItem => refItem.GUID_Parent != mediaItemType.GUID);
                var mediaItemsRefs = refItems.Where(refItem => refItem.GUID_Parent == mediaItemType.GUID);



                var dbReaderRefs = new OntologyModDBConnector(globals);

                var searchRefs = mediaItemsRefs.Select(refItem => new clsObjectRel
                {
                    ID_Object = refItem.GUID,
                    ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID
                }).ToList();



                var dbReaderMediaItems = new OntologyModDBConnector(globals);

                var searchMediaItems = refItemsNoMediaItems.Select(refItem => new clsObjectRel
                {
                    ID_Other = refItem.GUID,
                    ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID,
                    ID_Parent_Object = mediaItemType.GUID
                }
                ).ToList();

                if (searchRefs.Any())
                {
                    resultDbReader = dbReaderMediaItems.GetDataObjectRel(searchRefs);
                    if (resultDbReader.GUID == globals.LState_Error.GUID)
                    {
                        result.Result = globals.LState_Error.Clone();
                        return result;

                    }
                }

                if (searchMediaItems.Any())
                {
                    resultDbReader = dbReaderMediaItems.GetDataObjectRel(searchMediaItems);

                    if (resultDbReader.GUID == globals.LState_Error.GUID)
                    {
                        result.Result = globals.LState_Error.Clone();
                        return result;

                    }

                }

                result.MediaItemsToRef = dbReaderRefs.ObjectRels;
                result.MediaItemsToRef.AddRange(dbReaderMediaItems.ObjectRels);

                result.MediaItems = result.MediaItemsToRef.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Object,
                    Name = rel.Name_Object,
                    GUID_Parent = rel.ID_Parent_Object,
                    Type = globals.Type_Object
                }).ToList();

                result.RefItems = result.MediaItemsToRef.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = globals.Type_Other
                }).ToList();

                var dbReaderFiles = new OntologyModDBConnector(globals);

                var searchFiles = result.MediaItemsToRef.Select(medItmToRef => new clsObjectRel
                {
                    ID_Object = medItmToRef.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Media_Item_belonging_Source_File.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Media_Item_belonging_Source_File.ID_Class_Right
                }).ToList();

                resultDbReader = globals.LState_Success.Clone();

                if (searchFiles.Any())
                {
                    resultDbReader = dbReaderFiles.GetDataObjectRel(searchFiles);

                    if (resultDbReader.GUID == globals.LState_Error.GUID)
                    {
                        result.Result = globals.LState_Error.Clone();
                        return result;

                    }

                    result.MediaItemsToFiles = dbReaderFiles.ObjectRels;
                }
                else
                {
                    result.MediaItemsToFiles = new List<clsObjectRel>();
                }

                var dbReaderFileCreate = new OntologyModDBConnector(globals);

                resultDbReader = globals.LState_Success.Clone();

                var searchFilesToCreateDate = result.MediaItemsToFiles.Select(medToFile => new clsObjectAtt
                {
                    ID_Object = medToFile.ID_Other,
                    ID_AttributeType = Config.LocalData.ClassAtt_File_Datetimestamp__Create_.ID_AttributeType
                }).ToList();

                if (searchFilesToCreateDate.Any())
                {
                    resultDbReader = dbReaderFileCreate.GetDataObjectAtt(searchFilesToCreateDate);

                    if (resultDbReader.GUID == globals.LState_Error.GUID)
                    {
                        result.Result = globals.LState_Error.Clone();
                        return result;

                    }

                    result.FilesToCreateDate = dbReaderFileCreate.ObjAtts;
                }
                else
                {
                    result.FilesToCreateDate = new List<clsObjectAtt>();
                }

                var dbReader1 = new OntologyModDBConnector(globals);
                var dbReader2 = new OntologyModDBConnector(globals);

                resultDbReader = globals.LState_Success.Clone();

                var searchStart = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                        ID_Other = Config.LocalData.Object_Start.GUID
                    }
                };

                resultDbReader = dbReader1.GetDataObjectRel(searchStart, doIds: true);

                var searchBookmarksToMediaItems = dbReader1.ObjectRelsId.Select(bookMarks => new clsObjectRel
                {
                    ID_Object = bookMarks.ID_Object,
                    ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID,
                    ID_Parent_Other = Config.LocalData.Class_Media_Item.GUID
                }).ToList();

                if (searchBookmarksToMediaItems.Any())
                {
                    resultDbReader = dbReader2.GetDataObjectRel(searchBookmarksToMediaItems, doIds: true);

                    if (resultDbReader.GUID == globals.LState_Error.GUID)
                    {
                        result.Result = globals.LState_Error.Clone();
                        return result;
                    }
                    result.BookmarksToMediaItems = (from mediaItems in result.MediaItemsToRef
                                                    join mediaItemToBookmarks in dbReader2.ObjectRelsId on mediaItems.ID_Object equals mediaItemToBookmarks.ID_Other
                                                    join bookMarks in dbReader1.ObjectRelsId on mediaItemToBookmarks.ID_Object equals bookMarks.ID_Object
                                                    select mediaItemToBookmarks).ToList();
                }
                else
                {
                    result.BookmarksToMediaItems = new List<clsObjectRel>();
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> RelateMediaItems(List<clsOntologyItem> mediaItems, clsOntologyItem refItem, bool orderRelations)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
           {
               var result = new ResultItem<List<clsObjectRel>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsObjectRel>()
               };

               if (!mediaItems.Any())
               {
                   return result;
               }
               long orderId = 1;
               var relationConfig = new clsRelationConfig(globals);
               var rel = mediaItems.Select(mediaItem => relationConfig.Rel_ObjectRelation(mediaItem, refItem, Config.LocalData.RelationType_belongs_to, orderId: (orderRelations ? orderId++ : orderId))).ToList();

               var dbWriterRelations = new OntologyModDBConnector(globals);

               result.ResultState = dbWriterRelations.SaveObjRel(rel);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while saving Relations between MediaItems and Reference!";
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<PDFDocumentsRawResult>> GetPDFRawItems(List<PDFIdAndContent> pdfIds)
        {
            var taskResult = await Task.Run<ResultItem<PDFDocumentsRawResult>>(() =>
           {
               var result = new ResultItem<PDFDocumentsRawResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new PDFDocumentsRawResult()
               };

               var searchPDFDocuments = pdfIds.Select(pdf => new clsOntologyItem
               {
                   GUID = pdf.Id,
                   GUID_Parent = Config.LocalData.Class_PDF_Documents.GUID
               }).ToList();

               var dbReaderPDFDocuments = new OntologyModDBConnector(globals);

               if (searchPDFDocuments.Any())
               {
                   result.ResultState = dbReaderPDFDocuments.GetDataObjects(searchPDFDocuments);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the PDF-Documents!";
                       return result;
                   }
               }

               result.Result.PDFDocuments = dbReaderPDFDocuments.Objects1;

               var searchPDFDocsToReferences = dbReaderPDFDocuments.Objects1.Select(pdf => new clsObjectRel
               {
                   ID_Object = pdf.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_PDF_Documents_belongs_to.ID_RelationType,
               }).ToList();

               var dbReaderPDFDocsToReferences = new OntologyModDBConnector(globals);

               if (searchPDFDocsToReferences.Any())
               {
                   result.ResultState = dbReaderPDFDocsToReferences.GetDataObjectRel(searchPDFDocsToReferences);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the References!";
                       return result;
                   }
               }

               result.Result.RefItems = dbReaderPDFDocsToReferences.ObjectRels;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<OCRDocumentsRawResult>> GetOCRRawItems(List<OCRIdAndContent> ocrIds)
        {
            var taskResult = await Task.Run<ResultItem<OCRDocumentsRawResult>>(() =>
            {
                var result = new ResultItem<OCRDocumentsRawResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new OCRDocumentsRawResult()
                };

                var searchOCRDocuments = ocrIds.Select(ocr => new clsOntologyItem
                {
                    GUID = ocr.Id,
                    GUID_Parent = Config.LocalData.Class_PDF_Documents.GUID
                }).ToList();

                var dbReaderOCRDocuments = new OntologyModDBConnector(globals);

                if (searchOCRDocuments.Any())
                {
                    result.ResultState = dbReaderOCRDocuments.GetDataObjects(searchOCRDocuments);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the OCR-Documents!";
                        return result;
                    }
                }

                result.Result.OCRDocuments = dbReaderOCRDocuments.Objects1;

                var searchOCRDocsToReferences = dbReaderOCRDocuments.Objects1.Select(ocr => new clsObjectRel
                {
                    ID_Object = ocr.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Images__Graphic__belongs_to.ID_RelationType,
                }).ToList();

                var dbReaderOCRDocsToReferences = new OntologyModDBConnector(globals);

                if (searchOCRDocsToReferences.Any())
                {
                    result.ResultState = dbReaderOCRDocsToReferences.GetDataObjectRel(searchOCRDocsToReferences);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the References!";
                        return result;
                    }
                }

                result.Result.RefItems = dbReaderOCRDocsToReferences.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetFileItems(List<clsOntologyItem> searchFiles)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var dbReaderFileItems = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderFileItems.GetDataObjects(searchFiles);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the files!";
                    return result;
                }

                result.Result = dbReaderFileItems.Objects1;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> SaveNewMediaItems(List<clsOntologyItem> fileItems, AssociationType typeOfAssiciation)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
           {
               var result = new ResultItem<List<clsObjectRel>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsObjectRel>()
               };

               var relationConfig = new clsRelationConfig(globals);
               var mediaItemsToSave = fileItems.Select(file => new clsOntologyItem
               {
                   GUID = globals.NewGUID,
                   Name = file.Name,
                   GUID_Parent = typeOfAssiciation == AssociationType.Image ?
                        Config.LocalData.Class_Images__Graphic_.GUID :
                        typeOfAssiciation == AssociationType.MediaItem ?
                        Config.LocalData.Class_Media_Item.GUID :
                        Config.LocalData.Class_PDF_Documents.GUID,
                   Type = globals.Type_Object,
                   GUID_Related = file.GUID
               }).ToList();

               var dbWriterMediaItems = new OntologyModDBConnector(globals);

               result.ResultState = dbWriterMediaItems.SaveObjects(mediaItemsToSave);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while saving Media-Items";
                   return result;
               }

               var relsPre = (from fileItem in fileItems
                              join mediaItem in mediaItemsToSave on fileItem.GUID equals mediaItem.GUID_Related
                              select new { fileItem, mediaItem });

               var rels = relsPre.Select(rel => relationConfig.Rel_ObjectRelation(rel.mediaItem, rel.fileItem, Config.LocalData.RelationType_belonging_Source)).ToList();

               result.ResultState = dbWriterMediaItems.SaveObjRel(rels);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while saving Media-Items";
                   return result;
               }

               result.Result = rels;
               return result;
           });

            return taskResult;
        }

        public async Task<clsOntologyItem> CheckObjects(List<clsOntologyItem> objects)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbReaderObjects = new OntologyModDBConnector(globals);

                var searchObjects = objects.GroupBy(obj => obj.GUID_Parent).Select(obj => obj.Key).Select(obj => new clsOntologyItem
                {
                    GUID_Parent = obj
                }).ToList();
                result = dbReaderObjects.GetDataObjects(searchObjects);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while getting the objects!";
                    return result;
                }

                (from obj in objects
                 join dbObj in dbReaderObjects.Objects1 on obj.Name equals dbObj.Name into dbObjs
                 from dbObj in dbObjs.DefaultIfEmpty()
                 select new { obj, dbObj }).ToList().ForEach(item =>
                 {
                     if (item.dbObj != null)
                     {
                         item.obj.GUID = item.obj.GUID;
                         item.obj.New_Item = false;
                     }
                     else
                     {
                         item.obj.GUID = globals.NewGUID;
                         item.obj.New_Item = true;
                     }
                 });

                var itemsToCreate = objects.Where(obj => obj.New_Item.Value).ToList();

                var dbWriter = new OntologyModDBConnector(globals);

                if (itemsToCreate.Any())
                {
                    result = dbWriter.SaveObjects(itemsToCreate);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the objects!";
                        return result;
                    }
                }

                result.Additional1 = $"Existing items: {objects.Count(obj => !obj.New_Item.Value)} / Created Items: {objects.Count(obj => obj.New_Item.Value)}";
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetMediaPartner()
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                var searchAlbumPartner = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = Config.LocalData.ClassRel_Album_Artist_Partner.ID_Class_Left,
                        ID_RelationType = Config.LocalData.ClassRel_Album_Artist_Partner.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Album_Artist_Partner.ID_Class_Right
                    }
                };

                var dbReaderAblumPartner = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderAblumPartner.GetDataObjectRel(searchAlbumPartner);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the relations between the Albums and the Partners!";
                    return result;
                }

                result.Result = dbReaderAblumPartner.ObjectRels;

                var searchSongPartner = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = Config.LocalData.ClassRel_Song_Composer_Partner.ID_Class_Left,
                        ID_RelationType = Config.LocalData.ClassRel_Song_Composer_Partner.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Song_Composer_Partner.ID_Class_Right
                    }
                };

                var dbReaderSongPartner = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSongPartner.GetDataObjectRel(searchSongPartner);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the relations between the Songs and the Partners!";
                    return result;
                }

                result.Result.AddRange(dbReaderSongPartner.ObjectRels);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ResultMultimediaItems>> GetMultimediaItems(MediaItemType mediaItemType, List<clsOntologyItem> itemFilter, List<clsOntologyItem> related)
        {
            var taskResult = await Task.Run<ResultItem<ResultMultimediaItems>>(() =>
           {
               var result = new ResultItem<ResultMultimediaItems>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new ResultMultimediaItems()
               };

               var dbReader = new OntologyModDBConnector(globals);

               if (mediaItemType == MediaItemType.Audio)
               {
                   itemFilter.ForEach(item => item.GUID_Parent = Config.LocalData.Class_Media_Item.GUID);
               }
               else if (mediaItemType == MediaItemType.Image)
               {
                   itemFilter.ForEach(item => item.GUID_Parent = Config.LocalData.Class_Images__Graphic_.GUID);

               }
               else if (mediaItemType == MediaItemType.Video)
               {
                   itemFilter.ForEach(item => item.GUID_Parent = Config.LocalData.Class_Media_Item.GUID);
               }

               result.ResultState = dbReader.GetDataObjects(itemFilter);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the mediaItems";
                   return result;
               }
               result.Result.MediaItems = dbReader.Objects1;

               if (related != null && related.Any())
               {
                   var searchRelated = related.Select(rel => new clsObjectRel
                   {
                       ID_Other = rel.GUID,
                       ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID
                   }).ToList();

                   var dbReaderRelated = new OntologyModDBConnector(globals);
                   result.ResultState = dbReaderRelated.GetDataObjectRel(searchRelated);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the related!";
                       return result;
                   }

                   result.Result.MediaItems = (from mediaItem in result.Result.MediaItems
                                               join rel in dbReaderRelated.ObjectRels on mediaItem.GUID equals rel.ID_Object
                                               select mediaItem).ToList();
               }

               var dbReaderFiles = new OntologyModDBConnector(globals);

               var searchFiles = result.Result.MediaItems.Select(medItm => new clsObjectRel
               {
                   ID_Object = medItm.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Media_Item_belonging_Source_File.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Media_Item_belonging_Source_File.ID_Class_Right
               }).ToList();


               if (searchFiles.Any())
               {
                   result.ResultState = dbReaderFiles.GetDataObjectRel(searchFiles);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Files of MediaItems!";
                       return result;

                   }

                   result.Result.MediaItemsToFiles = dbReaderFiles.ObjectRels;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultMultimediaItems> SaveFileItems(clsOntologyItem refItem, List<clsOntologyItem> fileItems, string type)
        {
            var taskResult = await Task.Run<ResultMultimediaItems>(() =>
            {
                var result = new ResultMultimediaItems
                {
                    Result = globals.LState_Success.Clone()
                };

                var searchMultimediaItems = fileItems.Select(fileItem => new clsObjectRel
                {
                    ID_Other = fileItem.GUID,
                    ID_RelationType = Config.LocalData.RelationType_belonging_Source.GUID,
                    ID_Parent_Object = type
                }).ToList();

                var dbReaderMultimediaItems = new OntologyModDBConnector(globals);
                if (searchMultimediaItems.Any())
                {
                    result.Result = dbReaderMultimediaItems.GetDataObjectRel(searchMultimediaItems);
                }

                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                long orderId = 1;
                var mediaItemsPre = (from fileItem in fileItems
                                     join saved in dbReaderMultimediaItems.ObjectRels on fileItem.GUID equals saved.ID_Other into saveds
                                     from saved in saveds.DefaultIfEmpty()
                                     where saved == null
                                     select fileItem).Select(fileItem =>
                                     {
                                         var mediaItem = new clsOntologyItem
                                         {
                                             GUID = globals.NewGUID,
                                             Name = fileItem.Name,
                                             GUID_Parent = type,
                                             Type = globals.Type_Object,
                                             GUID_Related = fileItem.GUID
                                         };

                                         return mediaItem;
                                     });
                var mediaItems = new List<clsOntologyItem>(mediaItemsPre);

                var relsMedToFile = (from mediaItem in mediaItems
                                     join fileItem in fileItems on mediaItem.GUID_Related equals fileItem.GUID
                                     select relationConfig.Rel_ObjectRelation(mediaItem,
                                                fileItem, Config.LocalData.RelationType_belonging_Source, orderId: 1, full: true)).ToList();

                var relsMedToRefItem = mediaItems.Select(medItem => relationConfig.Rel_ObjectRelation(medItem,
                                                refItem, Config.LocalData.RelationType_belongs_to, orderId: orderId++, full: true)).ToList();

                var dbWriter = new OntologyModDBConnector(globals);
                if (mediaItems.Any())
                {
                    result.Result = dbWriter.SaveObjects(mediaItems);
                }


                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                if (relsMedToFile.Any())
                {
                    result.Result = dbWriter.SaveObjRel(relsMedToFile);
                }



                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    dbWriter.DelObjects(mediaItems);
                    return result;
                }

                if (relsMedToRefItem.Any())
                {
                    result.Result = dbWriter.SaveObjRel(relsMedToRefItem);
                }

                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    dbWriter.DelObjectRels(relsMedToFile);
                    dbWriter.DelObjects(mediaItems);
                    return result;
                }

                result.MediaItemsToFiles = relsMedToFile;
                result.MediaItemsToRef = relsMedToRefItem;
                result.MediaItems = mediaItems;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetMediaItemsToRefRelations(List<clsObjectRel> mediaItemToRef)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjectRel(mediaItemToRef.Select(rel => new clsObjectRel { ID_Object = rel.ID_Object, ID_Other = rel.ID_Other, ID_RelationType = rel.ID_RelationType }).ToList());

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the relation between MediaItem and Reference";
                    return result;
                }

                result.Result = dbReader.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> UpdateMediaListItems(List<clsObjectRel> mediaItemsToRefs, List<clsOntologyItem> mediaItems)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Nothing.Clone();

                var dbWriter = new OntologyModDBConnector(globals);

                if (mediaItemsToRefs.Any())
                {
                    result = dbWriter.SaveObjRel(mediaItemsToRefs);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving relation between MediaItem and Reference. MediaItems will not be changed!";
                        return result;
                    }
                }

                if (mediaItems.Any())
                {
                    result = dbWriter.SaveObjects(mediaItems);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving MediaItems!";
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetMultimediaItemsOfFiles(List<clsOntologyItem> files, AssociationType associationType)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
           {
               var result = new ResultItem<List<clsObjectRel>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsObjectRel>()
               };

               var searchMediaItemsToFiles = files.Select(file => new clsObjectRel
               {
                   ID_Other = file.GUID,
                   ID_RelationType = Config.LocalData.RelationType_belonging_Source.GUID,
                   ID_Parent_Object = associationType == AssociationType.Image ?
                         Config.LocalData.Class_Images__Graphic_.GUID :
                         associationType == AssociationType.MediaItem ?
                         Config.LocalData.Class_Media_Item.GUID :
                         Config.LocalData.Class_PDF_Documents.GUID

               }).ToList();

               var dbReaderMediaItemsToFiles = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderMediaItemsToFiles.GetDataObjectRel(searchMediaItemsToFiles);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relations between the files and the Multimedia-Items!";
                   return result;
               }

               result.Result = dbReaderMediaItemsToFiles.ObjectRels;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultMultimediaItems> SearchMediaItemsByMediaItemList(List<clsOntologyItem> mediaItems, clsOntologyItem mediaItemType)
        {

            var taskResult = await Task.Run<ResultMultimediaItems>(() =>
            {
                var result = new ResultMultimediaItems
                {
                    Result = globals.LState_Success.Clone()
                };

                var dbReaderMediaItems = new OntologyModDBConnector(globals);

                var searchMediaItems = mediaItems.Select(refItem => new clsObjectRel
                {
                    ID_Object = refItem.GUID,
                    ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID
                }
                ).ToList();

                clsOntologyItem resultDbReader = globals.LState_Success.Clone();

                if (searchMediaItems.Any())
                {
                    resultDbReader = dbReaderMediaItems.GetDataObjectRel(searchMediaItems);

                    if (resultDbReader.GUID == globals.LState_Error.GUID)
                    {
                        result.Result = globals.LState_Error.Clone();
                        return result;

                    }

                }

                result.MediaItemsToRef = dbReaderMediaItems.ObjectRels;
                result.RefItems = dbReaderMediaItems.ObjectRels.Select(mediaItmToRef => new clsOntologyItem
                {
                    GUID = mediaItmToRef.ID_Other,
                    Name = mediaItmToRef.Name_Other,
                    GUID_Parent = mediaItmToRef.ID_Parent_Other,
                    Type = mediaItmToRef.Ontology
                }).ToList();

                var dbReaderFiles = new OntologyModDBConnector(globals);

                var searchFiles = result.MediaItemsToRef.Select(medItmToRef => new clsObjectRel
                {
                    ID_Object = medItmToRef.ID_Object,
                    ID_RelationType = Config.LocalData.RelationType_belonging_Source.GUID,
                    ID_Parent_Other = Config.LocalData.Class_File.GUID
                }).ToList();

                resultDbReader = globals.LState_Success.Clone();

                if (searchFiles.Any())
                {
                    resultDbReader = dbReaderFiles.GetDataObjectRel(searchFiles);

                    if (resultDbReader.GUID == globals.LState_Error.GUID)
                    {
                        result.Result = globals.LState_Error.Clone();
                        return result;

                    }

                    result.MediaItemsToFiles = dbReaderFiles.ObjectRels;
                }
                else
                {
                    result.MediaItemsToFiles = new List<clsObjectRel>();
                }

                var dbReaderFileCreate = new OntologyModDBConnector(globals);

                resultDbReader = globals.LState_Success.Clone();

                var searchFilesToCreateDate = result.MediaItemsToFiles.Select(medToFile => new clsObjectAtt
                {
                    ID_Object = medToFile.ID_Other,
                    ID_AttributeType = Config.LocalData.ClassAtt_File_Datetimestamp__Create_.ID_AttributeType,
                }).ToList();

                if (searchFilesToCreateDate.Any())
                {
                    resultDbReader = dbReaderFileCreate.GetDataObjectAtt(searchFilesToCreateDate);

                    if (resultDbReader.GUID == globals.LState_Error.GUID)
                    {
                        result.Result = globals.LState_Error.Clone();
                        return result;

                    }

                    result.FilesToCreateDate = dbReaderFileCreate.ObjAtts;
                }
                else
                {
                    result.FilesToCreateDate = new List<clsObjectAtt>();
                }

                var dbReader1 = new OntologyModDBConnector(globals);
                var dbReader2 = new OntologyModDBConnector(globals);

                resultDbReader = globals.LState_Success.Clone();

                var searchStart = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = Config.LocalData.Class_Media_Item_Bookmark.GUID,
                        ID_Other = Config.LocalData.Object_Start.GUID
                    }
                };

                resultDbReader = dbReader1.GetDataObjectRel(searchStart, doIds: true);

                var searchBookmarksToMediaItems = dbReader1.ObjectRelsId.Select(bookMarks => new clsObjectRel
                {
                    ID_Object = bookMarks.ID_Object,
                    ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID,
                    ID_Parent_Other = Config.LocalData.Class_Media_Item.GUID
                }).ToList();

                if (searchBookmarksToMediaItems.Any())
                {
                    resultDbReader = dbReader2.GetDataObjectRel(searchBookmarksToMediaItems, doIds: true);

                    if (resultDbReader.GUID == globals.LState_Error.GUID)
                    {
                        result.Result = globals.LState_Error.Clone();
                        return result;
                    }
                    result.BookmarksToMediaItems = (from mediaItms in result.MediaItemsToRef
                                                    join mediaItemToBookmarks in dbReader2.ObjectRelsId on mediaItms.ID_Object equals mediaItemToBookmarks.ID_Other
                                                    join bookMarks in dbReader1.ObjectRelsId on mediaItemToBookmarks.ID_Object equals bookMarks.ID_Object
                                                    select mediaItemToBookmarks).ToList();
                }
                else
                {
                    result.BookmarksToMediaItems = new List<clsObjectRel>();
                }

                return result;
            });

            return taskResult;
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyAppDBConnector.OntologyModDBConnector(globals);
            return dbReader.GetOItem(id, type);
        }

        public async Task<ResultOItem<ClassObject>> GetClassObject(string idObject)
        {
            var taskResult = await Task.Run<ResultOItem<ClassObject>>(() =>
            {
                var result = new ResultOItem<ClassObject>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchObject = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = idObject
                    }
                };

                var dbReaderObject = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderObject.GetDataObjects(searchObject);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading Object";
                    return result;
                }

                if (!dbReaderObject.Objects1.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Reference provided";
                    return result;
                }

                result.Result = new ClassObject
                {
                    ObjectItem = dbReaderObject.Objects1.First().Clone()
                };

                var searchClass = dbReaderObject.Objects1.Select(obj => new clsOntologyItem
                {
                    GUID = obj.GUID_Parent
                }).ToList();

                result.ResultState = dbReaderObject.GetDataClasses(searchClass);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading Class";
                    return result;
                }

                if (!dbReaderObject.Classes1.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Class found";
                    return result;
                }

                result.Result.ClassItem = dbReaderObject.Classes1.First();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetObjects(List<string> objectIds)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsOntologyItem>()
               };

               var searchItems = objectIds.Select(objectId => new clsOntologyItem
               {
                   GUID = objectId
               }).ToList();

               var dbReader = new OntologyModDBConnector(globals);

               if (searchItems.Any())
               {
                   result.ResultState = dbReader.GetDataObjects(searchItems);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Objects!";
                       return result;
                   }
               }

               result.Result = dbReader.Objects1;

               return result;
           });
            return taskResult;
        }

        public async Task<ResultItem<GetImageMapModel>> GetImageMapModel(GetImageMapRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetImageMapModel>>(() =>
            {
                var result = new ResultItem<GetImageMapModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetImageMapModel()
                };

                var searchReference = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdReference
                    }
                };

                var dbReaderReference = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReference.GetDataObjects(searchReference);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Reference!";
                    return result;
                }

                result.Result.Reference = dbReaderReference.Objects1.FirstOrDefault();

                result.ResultState = ValidationController.ValidateGetImageMapModel(result.Result, globals, nameof(Models.GetImageMapModel.Reference));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                if (result.Result.Reference.GUID_Parent == ImageMap.Config.LocalData.Class_Image_Map.GUID)
                {
                    result.Result.ImageMap = result.Result.Reference;
                }
                else
                {
                    var searchImageMap = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Other = result.Result.Reference.GUID,
                            ID_RelationType = ImageMap.Config.LocalData.RelationType_belongs_to.GUID,
                            ID_Parent_Object = ImageMap.Config.LocalData.Class_Image_Map.GUID
                        }
                    };

                    var dbReaderImageMap = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderImageMap.GetDataObjectRel(searchImageMap);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the relation between the Image Map and the Reference!";
                        return result;
                    }

                    if (dbReaderImageMap.ObjectRels.Count > 1)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"You can only have on Image-Map on one Reference, but you have {dbReaderImageMap.ObjectRels.Count} relationes!";
                        return result;
                    }

                    var imageMapRel = dbReaderImageMap.ObjectRels.First();
                    result.Result.ImageMap = new clsOntologyItem
                    {
                        GUID = imageMapRel.ID_Object,
                        Name = imageMapRel.Name_Object,
                        GUID_Parent = imageMapRel.ID_Parent_Object,
                        Type = globals.Type_Object
                    };
                }

                result.ResultState = ValidationController.ValidateGetImageMapModel(result.Result, globals, nameof(Models.GetImageMapModel.ImageMap));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                if (result.Result.ImageMap != null)
                {
                    var searchImageAreas = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = result.Result.ImageMap.GUID,
                            ID_RelationType = ImageMap.Config.LocalData.ClassRel_Image_Map_contains_Image_Area.ID_RelationType,
                            ID_Parent_Other = ImageMap.Config.LocalData.ClassRel_Image_Map_contains_Image_Area.ID_Class_Right
                        }
                    };

                    var dbReaderImageAreas = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderImageAreas.GetDataObjectRel(searchImageAreas);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Image-Areas!";
                        return result;
                    }

                    result.Result.ImageMapsToImageAreas = dbReaderImageAreas.ObjectRels;

                    result.ResultState = ValidationController.ValidateGetImageMapModel(result.Result, globals, nameof(Models.GetImageMapModel.ImageMapsToImageAreas));

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var searchCoords = result.Result.ImageMapsToImageAreas.Select(rel => new clsObjectAtt
                    {
                        ID_Object = rel.ID_Other,
                        ID_AttributeType = ImageMap.Config.LocalData.AttributeType_Coords.GUID
                    }).ToList();

                    var dbReaderCoords = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderCoords.GetDataObjectAtt(searchCoords);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the the Coords of the Image-Areas!";
                        return result;
                    }

                    result.Result.ImageAreasToCoords = dbReaderCoords.ObjAtts;

                    result.ResultState = ValidationController.ValidateGetImageMapModel(result.Result, globals, nameof(Models.GetImageMapModel.ImageAreasToCoords));

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }


                    var searchShapes = result.Result.ImageMapsToImageAreas.Select(rel => new clsObjectRel
                    {
                        ID_Object = rel.ID_Other,
                        ID_RelationType = ImageMap.Config.LocalData.ClassRel_Image_Area_belonging_Shape__Image_Map_.ID_RelationType,
                        ID_Parent_Other = ImageMap.Config.LocalData.ClassRel_Image_Area_belonging_Shape__Image_Map_.ID_Class_Right
                    }).ToList();

                    var dbReaderShapes = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderShapes.GetDataObjectRel(searchShapes);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Shapes!";
                        return result;
                    }

                    result.Result.ImageAreasToShapes = dbReaderShapes.ObjectRels;

                    result.ResultState = ValidationController.ValidateGetImageMapModel(result.Result, globals, nameof(Models.GetImageMapModel.ImageAreasToShapes));

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var searchObjects = result.Result.ImageMapsToImageAreas.Select(rel => new clsObjectRel
                    {
                        ID_Object = rel.ID_Other,
                        ID_RelationType = ImageMap.Config.LocalData.ClassRel_Image_Area_belonging_Object.ID_RelationType
                    }).ToList();

                    var dbReaderObjects = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderObjects.GetDataObjectRel(searchObjects);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Objects!";
                        return result;
                    }

                    result.Result.ImageAreasToObjects = dbReaderObjects.ObjectRels;

                    result.ResultState = ValidationController.ValidateGetImageMapModel(result.Result, globals, nameof(Models.GetImageMapModel.ImageAreasToObjects));

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }
                

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<OCRImagesModel>> GetOCRImagesModel(OCRImagesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<OCRImagesModel>>(async () =>
            {
                var result = new ResultItem<OCRImagesModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new OCRImagesModel()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig,
                        GUID_Parent = OCR.Config.LocalData.Class_OCR_Images.GUID
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Index-Config!";
                    return result;
                }

                result.Result.Config = dbReaderConfig.Objects1.FirstOrDefault();

                result.ResultState = ValidationController.ValidateOCRImagesModel(result.Result, globals, nameof(result.Result.Config));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchPaths = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = OCR.Config.LocalData.ClassRel_OCR_Images_belonging_Source_Path.ID_RelationType,
                        ID_Parent_Other = OCR.Config.LocalData.ClassRel_OCR_Images_belonging_Source_Path.ID_Class_Right
                    }
                };

                var dbReaderPaths = new OntologyModDBConnector(globals);

                if (searchPaths.Any())
                {
                    result.ResultState = dbReaderPaths.GetDataObjectRel(searchPaths);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Path!";
                        return result;
                    }

                    result.Result.ConfigToPath = dbReaderPaths.ObjectRels.FirstOrDefault();
                }

                var esConfigConnector = new ElasticSearchConfigController(globals);

                var getEsIndexResult = await esConfigConnector.GetConfig(result.Result.Config, OCR.Config.LocalData.RelationType_belonging_Destination, OCR.Config.LocalData.RelationType_uses, globals.Direction_LeftRight);

                result.ResultState = getEsIndexResult.ResultState;

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Es-Index and the Es-Type!";
                    return result;
                }

                result.Result.ElasticSearchConfig = getEsIndexResult.Result;

                result.ResultState = ValidationController.ValidateOCRImagesModel(result.Result, globals, nameof(result.Result.ElasticSearchConfig));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                if (result.Result.ConfigToPath == null)
                {
                    var mediaViewerController = new MediaViewerModule.Connectors.MediaViewerConnector(globals);

                    var imagesResult = await mediaViewerController.GetImages(new List<clsOntologyItem>(), request.MessageOutput);

                    result.ResultState = imagesResult.Result;

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.Images = imagesResult.MediaListItems;

                    var searchImages = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Parent_Object = OCR.Config.LocalData.ClassRel_Index_Session__OCR__contains_Images__Graphic_.ID_Class_Left,
                            ID_RelationType = OCR.Config.LocalData.ClassRel_Index_Session__OCR__contains_Images__Graphic_.ID_RelationType,
                            ID_Parent_Other = OCR.Config.LocalData.ClassRel_Index_Session__OCR__contains_Images__Graphic_.ID_Class_Right
                        }
                    };

                    var dbReaderSessionPDFs = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderSessionPDFs.GetDataObjectRel(searchImages);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Session-Images!";
                        return result;
                    }

                    result.Result.SessionOCRs = dbReaderSessionPDFs.ObjectRels;

                    var searchSessionImagesNotExisting = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Parent_Object = OCR.Config.LocalData.ClassRel_Index_Session__OCR__not_existing_Images__Graphic_.ID_Class_Left,
                            ID_RelationType = OCR.Config.LocalData.ClassRel_Index_Session__OCR__not_existing_Images__Graphic_.ID_RelationType,
                            ID_Parent_Other = OCR.Config.LocalData.ClassRel_Index_Session__OCR__not_existing_Images__Graphic_.ID_Class_Right
                        }
                    };

                    var dbReaderNotExisting = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderNotExisting.GetDataObjectRel(searchSessionImagesNotExisting);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the not-existing Images!";
                        return result;
                    }

                    result.Result.SessionOCRsNotExisting = dbReaderNotExisting.ObjectRels;
                    var searchSessionImagesError = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Parent_Object = OCR.Config.LocalData.ClassRel_Index_Session__OCR__error_Images__Graphic_.ID_Class_Left,
                            ID_RelationType = OCR.Config.LocalData.ClassRel_Index_Session__OCR__error_Images__Graphic_.ID_RelationType,
                            ID_Parent_Other = OCR.Config.LocalData.ClassRel_Index_Session__OCR__error_Images__Graphic_.ID_Class_Right
                        }
                    };

                    var dbReaderError = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderError.GetDataObjectRel(searchSessionImagesError);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Error-PDFs!";
                        return result;
                    }

                    result.Result.SessionOCRsError = dbReaderError.ObjectRels;
                }


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> CheckObjectsByName(List<clsOntologyItem> objects)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = objects
                };

                var dbReader = new OntologyModDBConnector(globals);

                if (!objects.Any())
                {
                    return result;
                }

                result.ResultState = dbReader.GetDataObjects(objects);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the objects!";
                    return result;
                }

                foreach (var objToCheck in (from objCheck in objects
                                      join objDb in dbReader.Objects1 on objCheck.GUID equals objDb.GUID into objDbs
                                      from objDb in objDbs.DefaultIfEmpty()
                                      select new { objCheck, objDb }))
                {
                    if (objToCheck.objDb == null)
                    {
                        objToCheck.objCheck.Mark = true;
                        result.Result.Add(objToCheck.objCheck);
                    }
                    else if (objToCheck.objDb.Name != objToCheck.objCheck.Name)
                    {
                        objToCheck.objCheck.Mark = true;
                        result.Result.Add(objToCheck.objCheck);
                    }
                    else
                    {
                        result.Result.Add(objToCheck.objCheck);
                    }
                }
                

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<MediaBookMarkModel>> GetMediaBookmarkModel(GetMediaBookmarkModelRequest request)
        {
            var taskResult = await Task.Run<ResultItem<MediaBookMarkModel>>(() =>
           {
               var result = new ResultItem<MediaBookMarkModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new MediaBookMarkModel()
               };

               result.ResultState = ValidationController.ValidateGetMediaBookmarkModelRequest(request, globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               if (request.RequestType == MediaBookmarkModelRequestType.ByBookmarkIds)
               {
                   result = GetMediaBookmarkModelByBookmarkIds(request.Ids);
               }
               else
               {
                   result = GetMediaBookmarkModelRelations(request.Ids, request.RequestType);
               }
               
               return result;
           });

            return taskResult;
        }

        private ResultItem<MediaBookMarkModel> GetMediaBookmarkModelByBookmarkIds(List<string> ids)
        {
            var result = new ResultItem<MediaBookMarkModel>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new MediaBookMarkModel()
            };

            var dbReaderBookmarks = new OntologyModDBConnector(globals);

            var searchMediaBookmarks = ids.Select(id => new clsOntologyItem
            {
                GUID = id
            }).ToList();

            result.ResultState = dbReaderBookmarks.GetDataObjects(searchMediaBookmarks);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while getting the Bookmarks!";
                return result;
            }

            result.Result.MediaBookmarks = dbReaderBookmarks.Objects1;

            result.ResultState = ValidationController.ValidateMediaBookMarkModel(result.Result, globals, nameof(MediaBookMarkModel.MediaBookmarks));

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var attributesResult = GetAttributesOfMediabookMarks(result.Result.MediaBookmarks);

            result.ResultState = attributesResult.ResultState;

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            result.Result.MediaBookmarkCreationDates = attributesResult.Result.Where(att => att.ID_AttributeType == Config.LocalData.AttributeType_Datetimestamp__Create_.GUID).ToList();
            result.Result.MediaBookmarkSecond = attributesResult.Result.Where(att => att.ID_AttributeType == Config.LocalData.AttributeType_Second.GUID).ToList();

            result.ResultState = GetMediaBookmarkModelRelations(result.Result, MediaBookmarkModelRequestType.ByBookmarkIds);
            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            return result;
        }

        private ResultItem<MediaBookMarkModel> GetMediaBookmarkModelRelations(List<string> ids, MediaBookmarkModelRequestType requestType)
        {
            var result = new ResultItem<MediaBookMarkModel>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new MediaBookMarkModel()
            };


            var searchRelations = new List<clsObjectRel>();
            switch(requestType)
            {
                case MediaBookmarkModelRequestType.ByLogstateIds:
                    searchRelations = ids.Select(id => new clsObjectRel
                    {
                        ID_Other = id,
                        ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_is_of_Type_Logstate.ID_RelationType,
                        ID_Parent_Object = Config.LocalData.ClassRel_Media_Item_Bookmark_is_of_Type_Logstate.ID_Class_Left
                    }).ToList();
                    break;
                case MediaBookmarkModelRequestType.ByMediaItemIds:
                    searchRelations = ids.Select(id => new clsObjectRel
                    {
                        ID_Other = id,
                        ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_belongs_to_Media_Item.ID_RelationType,
                        ID_Parent_Object = Config.LocalData.ClassRel_Media_Item_Bookmark_belongs_to_Media_Item.ID_Class_Left
                    }).ToList();
                    break;
                case MediaBookmarkModelRequestType.ByReferenceIds:
                    searchRelations = ids.Select(id => new clsObjectRel
                    {
                        ID_Other = id,
                        ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_is_used_by.ID_RelationType
                    }).ToList();
                    break;
                case MediaBookmarkModelRequestType.ByUserIds:
                    searchRelations = ids.Select(id => new clsObjectRel
                    {
                        ID_Other = id,
                        ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_belongs_to_user.ID_RelationType,
                        ID_Parent_Object = Config.LocalData.ClassRel_Media_Item_Bookmark_belongs_to_user.ID_Class_Left
                    }).ToList();
                    break;
            }
            var dbReaderRelations = new OntologyModDBConnector(globals);

            if (searchRelations.Any())
            {
                result.ResultState = dbReaderRelations.GetDataObjectRel(searchRelations);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = ERROR_LOGSTATES;
                    return result;
                }
            }
            
            switch (requestType)
            {
                case MediaBookmarkModelRequestType.ByLogstateIds:
                    result.Result.MediaBookmarksToLogStates = dbReaderRelations.ObjectRels;
                    break;
                case MediaBookmarkModelRequestType.ByMediaItemIds:
                    result.Result.MediaBookmarksToMediaItems = dbReaderRelations.ObjectRels;
                    break;
                case MediaBookmarkModelRequestType.ByReferenceIds:
                    result.Result.MediaBookmarksToReferences = dbReaderRelations.ObjectRels;
                    break;
                case MediaBookmarkModelRequestType.ByUserIds:
                    result.Result.MediaBookmarksToUsers = dbReaderRelations.ObjectRels;
                    break;
            }
            
            result.Result.MediaBookmarks = dbReaderRelations.ObjectRels.GroupBy(rel => new { rel.ID_Object, rel.Name_Object, rel.ID_Parent_Object }).Select(grp => new clsOntologyItem
            {
                GUID = grp.Key.ID_Object,
                Name = grp.Key.Name_Object,
                GUID_Parent = grp.Key.ID_Parent_Object,
                Type = globals.Type_Object
            }).ToList();

            result.ResultState = ValidationController.ValidateMediaBookMarkModel(result.Result, globals, nameof(MediaBookMarkModel.MediaBookmarks));

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var attributesResult = GetAttributesOfMediabookMarks(result.Result.MediaBookmarks);

            result.ResultState = attributesResult.ResultState;

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            result.Result.MediaBookmarkCreationDates = attributesResult.Result.Where(att => att.ID_AttributeType == Config.LocalData.AttributeType_Datetimestamp__Create_.GUID).ToList();
            result.Result.MediaBookmarkSecond = attributesResult.Result.Where(att => att.ID_AttributeType == Config.LocalData.AttributeType_Second.GUID).ToList();

            result.ResultState = GetMediaBookmarkModelRelations(result.Result, requestType);
            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            return result;
        }

        private clsOntologyItem GetMediaBookmarkModelRelations(MediaBookMarkModel model, MediaBookmarkModelRequestType requestType)
        {
            var result = globals.LState_Success.Clone();

            var searchRelations = new List<clsObjectRel>();

            if (requestType != MediaBookmarkModelRequestType.ByMediaItemIds)
            {
                searchRelations.AddRange(model.MediaBookmarks.Select(bookMark => new clsObjectRel
                {
                    ID_Object = bookMark.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_belongs_to_Media_Item.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Media_Item_Bookmark_belongs_to_Media_Item.ID_Class_Right
                }));
            }

            if (requestType != MediaBookmarkModelRequestType.ByUserIds)
            {
                searchRelations.AddRange(model.MediaBookmarks.Select(bookMark => new clsObjectRel
                {
                    ID_Object = bookMark.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_belongs_to_user.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Media_Item_Bookmark_belongs_to_user.ID_Class_Right
                }));
            }

            if (requestType != MediaBookmarkModelRequestType.ByLogstateIds)
            {
                searchRelations.AddRange(model.MediaBookmarks.Select(bookMark => new clsObjectRel
                {
                    ID_Object = bookMark.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_is_of_Type_Logstate.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Media_Item_Bookmark_is_of_Type_Logstate.ID_Class_Right
                }));
            }

            if (requestType != MediaBookmarkModelRequestType.ByReferenceIds)
            {
                searchRelations.AddRange(model.MediaBookmarks.Select(bookMark => new clsObjectRel
                {
                    ID_Object = bookMark.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Media_Item_Bookmark_is_used_by.ID_RelationType,
                }));
            }

            var dbReader = new OntologyModDBConnector(globals);

            if (searchRelations.Any())
            {
                result = dbReader.GetDataObjectRel(searchRelations);
                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = ERROR_LOGSTATES;
                    return result;
                }
            }

            if (requestType != MediaBookmarkModelRequestType.ByLogstateIds)
            {
                model.MediaBookmarksToLogStates = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Logstate.GUID).ToList();
            }

            if (requestType != MediaBookmarkModelRequestType.ByMediaItemIds)
            {
                model.MediaBookmarksToMediaItems = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Media_Item.GUID).ToList();
            }

            if (requestType != MediaBookmarkModelRequestType.ByUserIds)
            {
                model.MediaBookmarksToUsers = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_user.GUID).ToList();
            }

            if (requestType != MediaBookmarkModelRequestType.ByReferenceIds)
            {
                model.MediaBookmarksToReferences = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == Config.LocalData.RelationType_is_used_by.GUID).ToList();
            }
                
            return result;
        }

        private ResultItem<List<clsObjectAtt>> GetAttributesOfMediabookMarks(List<clsOntologyItem> bookMarks)
        {
            var result = new ResultItem<List<clsObjectAtt>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectAtt>()
            };

            var dbReaderAttributes = new OntologyModDBConnector(globals);
            var searchAttributes = bookMarks.Select(bookMark => new clsObjectAtt
            {
                ID_Object = bookMark.GUID,
                ID_AttributeType = Config.LocalData.AttributeType_Datetimestamp__Create_.GUID
            }).ToList();

            searchAttributes.AddRange(bookMarks.Select(bookMark => new clsObjectAtt
            {
                ID_Object = bookMark.GUID,
                ID_AttributeType = Config.LocalData.AttributeType_Second.GUID
            }));

            if (searchAttributes.Any())
            {
                result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Attributes of the Bookmarks!";
                    return result;
                }
            }

            result.Result = dbReaderAttributes.ObjAtts;

            return result;
        }

        public ElasticAgentService(Globals globals) : base(globals)
        {
            relationConfig = new clsRelationConfig(globals);
        }
    }
}
