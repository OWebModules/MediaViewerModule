﻿using MediaViewerModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaViewerModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateGetImageMapRequest(GetImageMapRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(request.IdReference))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The IdReference is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdReference))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The IdReference is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateGetImageMapModel(GetImageMapModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetImageMapModel.Reference))
            {
                if (model.Reference == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The Reference is null!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetImageMapModel.ImageMapsToImageAreas))
            {
                if (model.ImageMap != null && !model.ImageMapsToImageAreas.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The ImageMap has no Image-Areas!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetImageMapModel.ImageAreasToCoords))
            {
                if (model.ImageMapsToImageAreas.Count != model.ImageAreasToCoords.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have {model.ImageMapsToImageAreas.Count} Areas and {model.ImageAreasToCoords.Count} Coords!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetImageMapModel.ImageAreasToShapes))
            {
                if (model.ImageMapsToImageAreas.Count != model.ImageAreasToShapes.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have {model.ImageMapsToImageAreas.Count} Areas and {model.ImageAreasToShapes.Count} Shapes!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetImageMapModel.ImageAreasToObjects))
            {
                if (model.ImageMapsToImageAreas.Count != model.ImageAreasToObjects.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have {model.ImageMapsToImageAreas.Count} Areas and {model.ImageAreasToObjects.Count} Objects!";
                    return result;
                }

                if (!model.ImageAreasToObjects.All(obj => obj.Ontology == globals.Type_Object))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have shapes with related items which are no Objects!";
                    return result;
                }
            }
            return result;
        }

        public static clsOntologyItem ValidateOCRImagesRequest(OCRImagesRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Success.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Success.Clone();
                result.Additional1 = "IdConfig is no valid Id!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateOCRImagesModel(OCRImagesModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || (propertyName == nameof(model.Config)))
            {
                if (model.Config == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The Config is not found!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || (propertyName == nameof(model.ElasticSearchConfig)))
            {
                if (model.ElasticSearchConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The Es-Index of Config is not found!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateGetMediaBookmarkModelRequest(GetMediaBookmarkModelRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (!request.Ids.Any())
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The list of Ids is empty!";
                return result;
            }

            if (!request.Ids.All(id => globals.is_GUID(id)))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You provided at least one invalid Id!";
                return result;
            }

            return result;
        }
        public static List<ResultItem<MediaBookmark>> GetValidInvalidMediaBookmarks(List<MediaBookmark> mediaBookmarks, Globals globals)
        {
            var result = new List<ResultItem<MediaBookmark>>();
            foreach (var bookMark in mediaBookmarks)
            {
                if (bookMark.IdMediaItem == null ||
                    bookMark.LogState == null ||
                    bookMark.LogState.GUID_Parent != Config.LocalData.Class_Logstate.GUID ||
                    bookMark.User == null ||
                    bookMark.User.GUID_Parent != Config.LocalData.Class_user.GUID)
                {
                    var resultState = globals.LState_Error.Clone();
                    resultState.Additional1 = "Bookmark is invalid!";
                    result.Add(new ResultItem<MediaBookmark>
                    {
                        ResultState = resultState,
                        Result = bookMark
                    });
                }
                else
                {
                    result.Add(new ResultItem<MediaBookmark>
                    {
                        ResultState = globals.LState_Success.Clone(),
                        Result = bookMark
                    });
                }
            }

            return result;

        }
        public static clsOntologyItem ValidateMediaBookMarkModel(MediaBookMarkModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();
            if (propertyName == null || propertyName == nameof(MediaBookMarkModel.MediaBookmarks))
            {
                if (!model.MediaBookmarks.All(bookmark => bookmark.GUID_Parent == Config.LocalData.Class_Media_Item_Bookmark.GUID))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "At least one Object in the list of Mediabookmarks is no Mediabookmark!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateRelateBookmarkRequest(RelateBookmarkRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var countWithNoMediaBookmark = request.BookmarkRelationsUpdate.Count(relUpd => relUpd.MediaBookmark == null);
            if (countWithNoMediaBookmark > 0 )
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"There are {countWithNoMediaBookmark} Update-Objects without an Bookmark!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateRemoveBookmarkRequest(RemoveReferencesRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (!request.MediaBookmarkReferences.Any())
            {
                return result;
            }

            if (request.MediaBookmarkReferences.Any(refItm => string.IsNullOrEmpty( refItm.IdMediaBookmark) || string.IsNullOrEmpty(refItm.IdReference)))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "One of the Reference-requests are invalid!";
                return result;
            }
            return result;
        }
    }
}
